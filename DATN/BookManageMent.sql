﻿USE master
GO
IF EXISTS(SELECT * FROM sys.databases WHERE name='ManagementBook')
DROP DATABASE ManagementBook
GO
CREATE DATABASE ManagementBook
GO
USE ManagementBook
GO

Create table [Accounts]
(
	[AccountID] Integer Identity NOT NULL,
	[UserName] Nvarchar(50) NULL,
	[PassWord] Nvarchar(50) NULL,
	[Name] Nvarchar(50) NULL,
	[PhoneNumber] Varchar(10) NULL,
	[Address] Nvarchar(50) NULL,
	[Email] Nvarchar(50) NULL,
	[Status] Tinyint NULL, -- 0 : NV , 1 AD , 2 VHH
Primary Key ([AccountID])
) 
go

Create table [Clients]
(
	[ClientID] Integer Identity NOT NULL,
	[ClientName] Nvarchar(50) NULL,
	[PhoneNumber] Varchar(10) NULL,
	[ClientAddress] Nvarchar(50) NULL,
	[Email] Nvarchar(50) NOT NULL, 
Primary Key ([ClientID])
) 
go

Create table [BillOfSales]
(
	[TransactionID] Integer Identity NOT NULL,
	[AccountID] Integer NOT NULL,
	[ClientID] Integer NOT NULL,
	[DateSale] Datetime NOT NULL,
Primary Key ([TransactionID])
) 
go

Create table [Books]
(
	[BookID] Integer Identity NOT NULL,
	[BookName] Nvarchar(50) NULL,
	[Price] Integer NULL,
	[Quantity] INT NOT NULL,
	[Status] Tinyint NULL,
	[CategoryID] Integer NOT NULL,
Primary Key ([BookID])
) 
go

Create table [BillDetails]
(
	[TransactionID] Integer NOT NULL,
	[BookID] Integer NOT NULL,
	[Quantity] Integer NULL,
	[SalePrice] Integer NULL,
Primary Key ([TransactionID],[BookID])
) 
go

Create table [Suppliers]
(
	[SupplierID] Integer Identity NOT NULL,
	[SupplierName] Nvarchar(50) NULL,
	[SupplierPhone] Nvarchar(10) NULL,
	[SupplierAddress] Nvarchar(50) NULL,
	[Email] Nvarchar(50) NULL, 
	[Status] Tinyint NULL,
Primary Key ([SupplierID])
) 
go

Create table [Categories]
(
	[CategoryID] Integer Identity NOT NULL,
	[CategoryName] Nvarchar(50) NULL,
Primary Key ([CategoryID])
) 
go
go
Create table [BillImports]
(
	[TransactionID] Integer Identity NOT NULL ,
	[AccountID] Integer NOT NULL,
	[PublisherID] Integer NOT NULL REFERENCES [Suppliers]([SupplierID]),
	[CreatedDate] Datetime NULL
Primary Key ([TransactionID])
) 
GO
Create table [ImportBillDetails]
(
	[TransactionID] Integer NOT NULL,
	[BookID] Integer NOT NULL,
	[Quantity] Integer NULL,
	[ImportPrice] Integer NULL,
Primary Key ([TransactionID],[BookID])
) 

go




Alter table [BillOfSales] add  foreign key([AccountID]) references [Accounts] ([AccountID])  on update no action on delete no action 
go
Alter table [BillImports] add  foreign key([AccountID]) references [Accounts] ([AccountID])  on update no action on delete no action 
go
Alter table [BillOfSales] add  foreign key([ClientID]) references [Clients] ([ClientID])  on update no action on delete no action 
go
Alter table [BillDetails] add  foreign key([TransactionID]) references [BillOfSales] ([TransactionID])  on update no action on delete no action 
go
Alter table [BillDetails] add  foreign key([BookID]) references [Books] ([BookID])  on update no action on delete no action 
go
Alter table [Books] add  foreign key([CategoryID]) references [Categories] ([CategoryID])  on update no action on delete no action 
go
Alter table [ImportBillDetails] add  foreign key([BookID]) references [Books] ([BookID])  on update no action on delete no action 
go
Alter table [ImportBillDetails] add  foreign key([TransactionID]) references [BillImports] ([TransactionID])  on update no action on delete no action 
go



Set quoted_identifier on
go


Set quoted_identifier off
go


/* Roles permissions */


/* Users permissions */



/* Roles permissions */


/* Users permissions */


INSERT INTO dbo.Accounts(UserName,PassWord,Name,PhoneNumber,Address,Email,Status)
					VALUES(   N'1', N'1',N'Nguyễn Văn Cường', '0866653231', N'Chí Tân - Khoái Châu - Hưng Yên',N'0902243870cuong@gmail.com', 1   )
					,(   N'2', N'2',N'Đào Thanh Hảo', '0866653231', N'Chí Tân - Khoái Châu - Hưng Yên',N'0902243870cuong@gmail.com', 0  )
					,(   N'3', N'3',N'Nguyễn Thị Trang', '0866653231', N'Chí Tân - Khoái Châu - Hưng Yên',N'Email3@gmail.com', 2 )
GO
INSERT INTO dbo.Categories(CategoryName)
				VALUES(N'Sách giáo khoa')
				,(N'Sách thiếu nhi')
				,(N'Sách bài tập')
				,(N'Truyện dân gian')
				,(N'Tiểu thuyết')
				,(N'Truyện phiếm')
				,(N'Chuyện cổ tích')
				,(N'Truyện ngụ ngôn')
				,(N'Truyện tranh')
				,(N'Khác')
GO
INSERT INTO dbo.Clients(ClientName,PhoneNumber,ClientAddress,Email)
VALUES(   N'Nguyễn Văn Cường', '0866653231', N'Chí Tân- Khoái Châu - Hưng Yên', N'0902243870cuong@gmail.com'  )
,(   N'Đào Thị Thanh Hảo', '0123456789', N'Chí Tân- Khoái Châu - Hưng Yên', N'0902243870cuong@gmail.com'  )
,(   N'Trần Quang Đạt', '0123456789', N'Chí Tân- Khoái Châu - Hưng Yên', N'0902243870cuong@gmail.com'  )
,(   N'Nguyễn Mạnh Cường', '0123456789', N'Chí Tân- Khoái Châu - Hưng Yên', N'0902243870cuong@gmail.com'  )
,(   N'Vũ Tùng Lâm', '0987654311', N'Chí Tân- Khoái Châu - Hưng Yên', N'0902243870cuong@gmail.com'  )
 GO
 INSERT INTO dbo.Books(BookName,Price,Quantity,Status,     CategoryID)
			 VALUES(   N'Toán lớp 1',10000,1000,  0,   1   )
			 ,(   N'Tiếng Việt 1',20000, 1000,  0,   1   )
			 ,(   N'Tự nhiên và xã hội',50000, 1000,  0,   1   )
			 ,(   N'Rước đèn ông sao',50000,1000,   0,   2   )
			 ,(   N'Sống lẽ phép',15000, 1000,  0,   2   )
			 ,(   N'Sách giải trí',25000,1000,   0,   2  )
			  ,(   N'BT Toán lớp 1',10000, 1000,  0,   3  )
			 ,(   N'BT Tiếng Việt 1',20000, 1000,  0,   3   )
			 ,(   N' BT Tự nhiên và xã hội',50000, 1000,  0,   3   )
			 ,(   N'Thánh Gióng',250000, 1000,  0,   4   )
			 ,(   N'Chử Đồng Tử Tiên Dung',50000,1000,   0,   4   )
			 ,(   N'Lạc Long QUân',50000,1000,   0,   4   )
---
GO
INSERT INTO [Suppliers] VALUES 
						(N'Nhà cung cấp Nhi Đồng','0123456789',N'Hà Nội','0902243870cuong@gmail.com',0)
						,(N'Nhà cung cấp Thiếu Nhi','0123456789',N'Hà Nội','0902243870cuong@gmail.com',0)
						,(N'Nhà cung cấp Giáo dục','0123456789',N'Hà Nội','0902243870cuong@gmail.com',0)
						,(N'Nhà cung cấp Bầu Trời','0123456789',N'Hà Nội','0902243870cuong@gmail.com',0)
						,(N'Nhà cung cấp Xanh Lam','0123456789',N'Hà Nội','0902243870cuong@gmail.com',0)
						
GO
SELECT * FROM dbo.Clients
SELECT * FROM dbo.Books
SELECT * FROM dbo.Categories
SELECT * FROM dbo.Accounts

select * from suppliers
