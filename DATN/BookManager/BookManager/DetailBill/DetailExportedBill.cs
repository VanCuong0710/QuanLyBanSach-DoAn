﻿using BookManager.Models;
using BookManager.OtherClass;
using iTextSharp.text;// THƯ VIỆN LƯU FILE PDF
using iTextSharp.text.pdf;// THƯ VIỆN LƯU FILE PDF
using System;
using System.Collections.Generic;
using System.IO;// THƯ VIỆN LƯU FILE PDF
using System.Windows.Forms;

namespace BookManager.DetailBill
{
    public partial class DetailExportedBill : Form
    {
        public DetailExportedBill()
        {
            InitializeComponent();
        }

        public int TransactionID { get; set; } = 1;
        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void DetailExportedBill_Load(object sender, EventArgs e)
        {
            txtMhd.Text = TransactionID.ToString();
            var account = mydb.BillOfSales.Find(TransactionID).Account;
            txtNguoiLap.Text = account.Name;
            dtpSale.Value = mydb.BillOfSales.Find(TransactionID).DateSale;
            var client = mydb.BillOfSales.Find(TransactionID).Client;
            txtNxb.Text = client.ClientName; ;
            txtSDT.Text = client.PhoneNumber;
            txtDc.Text = client.ClientAddress;
            var exportDetails = mydb.BillOfSales.Find(TransactionID).BillDetails;
            List<BookSales> bookSales = new List<BookSales>();
            foreach (var item in exportDetails)
            {
                BookSales bookSale = new BookSales();
                bookSale.ID = item.BookID;
                bookSale.Name = item.Book.BookName;
                bookSale.Category = item.Book.Category.CategoryName;
                bookSale.Price = (int)item.SalePrice;
                bookSale.Quantity = (int)item.Quantity;
                bookSales.Add(bookSale);
            }
            foreach (var item in bookSales)
            {
                dgvDshang.Rows.Add(item.ID, item.Name, item.Category, item.Quantity, item.Price, item.TotalPrice);
            }
            OtherFunction.SetDataGridView(dgvDshang);
            OtherFunction.SetFormatMoney(dgvDshang, 4);
            OtherFunction.SetFormatMoney(dgvDshang, 5);
            tinhTongTien();
        }

        private void tinhTongTien()
        {
            int tongtien = 0;
            for (int i = 0; i < dgvDshang.RowCount; i++)
            {
                string tien = dgvDshang.Rows[i].Cells[5].Value.ToString();
                tien = tien.Replace("VNĐ", "");
                tien = tien.Replace(" ", "");
                tongtien = tongtien + Convert.ToInt32(tien);
            }
            txtTongTien.Text = tongtien.ToString();
            OtherFunction.FormatTextbox(txtTongTien);
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            String filename = "HD" + txtMhd.Text;
            var savefile = new SaveFileDialog();
            savefile.FileName = filename;
            savefile.DefaultExt = ".pdf";
            if (savefile.ShowDialog() == DialogResult.OK)
            { // set up để thêm dữ liệu
                Document document = new Document(PageSize.A4, 10f, 20f, 20f, 20f);
                Stream stream = new FileStream(savefile.FileName, FileMode.Create);
                PdfWriter.GetInstance(document, stream);
                document.Open();

                PdfPTable table = new PdfPTable(3);
                float[] columnsWidth = { 1, 1, 1 };
                table.SetWidths(columnsWidth);
                table.DefaultCell.BorderWidth = 0;
                table.DefaultCell.Padding = 10;
                table.WidthPercentage = 100;
                table.HorizontalAlignment = Element.ALIGN_CENTER;
                // font chữ
                string path = Path.Combine(Path.GetFullPath(@"..\..\"), "Resources") + @"\font.ttf";
                BaseFont baseFont = BaseFont.CreateFont(path, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                //  Font font = new Font("Times New Roman", 12.0f);
                //BaseFont baseFont = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1252, BaseFont.EMBEDDED);

                iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 13, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontHearder = new iTextSharp.text.Font(baseFont, 20, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontBold = new iTextSharp.text.Font(baseFont, 14, iTextSharp.text.Font.BOLD);

                Phrase phrase2 = new Phrase("Chi tiết hóa đơn xuất", fontHearder);
                PdfPCell pdfPCell2 = new PdfPCell(phrase2);
                pdfPCell2.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell2.Colspan = 3;
                pdfPCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfPCell2.PaddingBottom = 25;
                pdfPCell2.PaddingLeft = 20;
                table.AddCell(pdfPCell2);

                Phrase phrase3 = new Phrase("Cửa hàng Ministop. Số điện thoại: 0123456789", fontBold);
                PdfPCell pdfPCell3 = new PdfPCell(phrase3);
                pdfPCell3.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell3.Colspan = 3;
                pdfPCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfPCell3.PaddingBottom = 5;
                pdfPCell3.PaddingLeft = 5;
                table.AddCell(pdfPCell3);

                Phrase phrase4 = new Phrase("Email: Cuongcalmtodoit@Gmail.com", fontBold);
                PdfPCell pdfPCell4 = new PdfPCell(phrase4);
                pdfPCell4.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell4.Colspan = 3;
                pdfPCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfPCell4.PaddingBottom = 5;
                pdfPCell4.PaddingLeft = 5;
                table.AddCell(pdfPCell4);

                Phrase phrase1 = new Phrase("Thông tin hóa đơn ", fontBold);
                PdfPCell pdfPCell1 = new PdfPCell(phrase1);
                pdfPCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell1.Colspan = 3;
                pdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfPCell1.PaddingBottom = 50;
                pdfPCell1.PaddingLeft = 5;
                table.AddCell(pdfPCell1);

                Phrase phrase5 = new Phrase($"Mã hóa đơn: {txtMhd.Text}                                                                             Tên khách hàng: {txtNxb.Text}", font);
                PdfPCell pdfPCell5 = new PdfPCell(phrase5);
                pdfPCell5.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell5.Colspan = 3;
                pdfPCell5.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                pdfPCell5.PaddingLeft = 5;
                table.AddCell(pdfPCell5);

                Phrase phrase6 = new Phrase($"Người lập: {txtNguoiLap.Text}                                                      Số điện thoại {txtSDT.Text}", font);
                PdfPCell pdfPCell6 = new PdfPCell(phrase6);
                pdfPCell6.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell6.Colspan = 3;
                pdfPCell6.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                pdfPCell6.PaddingLeft = 5;
                table.AddCell(pdfPCell6);

                Phrase phrase7 = new Phrase($"Ngày lập: {dtpSale.Text}                                                                 Địa chỉ: {txtDc.Text}", font);
                PdfPCell pdfPCell7 = new PdfPCell(phrase7);
                pdfPCell7.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell7.Colspan = 3;
                pdfPCell7.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                pdfPCell7.PaddingLeft = 5;
                pdfPCell7.PaddingBottom = 50;
                table.AddCell(pdfPCell7);

                Phrase phrase10 = new Phrase($"Danh sách hàng: ", font);
                PdfPCell pdfPCell10 = new PdfPCell(phrase10);
                pdfPCell10.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell10.Colspan = 3;
                pdfPCell10.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                pdfPCell10.PaddingLeft = 5;
                pdfPCell10.PaddingBottom = 5;
                table.AddCell(pdfPCell10);

                // số hóa đơn
                //tên

                //table Sản phẩm
                PdfPTable tableProduct = new PdfPTable(dgvDshang.ColumnCount);
                tableProduct.DefaultCell.PaddingBottom = 10;
                tableProduct.DefaultCell.PaddingTop = 10;
                tableProduct.WidthPercentage = 90;
                tableProduct.HorizontalAlignment = Element.ALIGN_LEFT;
                tableProduct.DefaultCell.BorderWidth = 1;

                //add headertext
                foreach (DataGridViewColumn column in dgvDshang.Columns)
                {
                    PdfPCell pdfPCell = new PdfPCell(new Phrase(column.HeaderText, fontBold));
                    pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPCell.PaddingBottom = 2;
                    tableProduct.AddCell(pdfPCell);
                }

                // add cell
                foreach (DataGridViewRow row in dgvDshang.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        PdfPCell pdfPCell = new PdfPCell(new Phrase(cell.Value.ToString(), font));
                        pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPCell.PaddingBottom = 2;
                        tableProduct.AddCell(pdfPCell);
                    }
                }

                PdfPTable table1 = new PdfPTable(3);
                float[] columnsWidth1 = { 1, 1, 1 };
                table.SetWidths(columnsWidth1);
                table.DefaultCell.BorderWidth = 0;
                table.DefaultCell.Padding = 10;
                table.WidthPercentage = 100;
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                Phrase phrase8 = new Phrase($"Tổng tiền: {txtTongTien.Text}", font);
                PdfPCell pdfPCell8 = new PdfPCell(phrase8);
                pdfPCell8.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell8.Colspan = 3;
                pdfPCell8.HorizontalAlignment = Element.ALIGN_RIGHT;
                pdfPCell8.PaddingTop = 15;
                pdfPCell8.PaddingLeft = 5;
                pdfPCell8.PaddingBottom = 50;
                table1.AddCell(pdfPCell8);

                Phrase phrase9 = new Phrase($"Chữ ký bên nhận                                                                           Chữ ký bên giao", font);
                PdfPCell pdfPCell9 = new PdfPCell(phrase9);
                pdfPCell9.Border = iTextSharp.text.Rectangle.NO_BORDER;
                pdfPCell9.Colspan = 3;
                pdfPCell9.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                pdfPCell9.PaddingLeft = 5;
                pdfPCell8.PaddingTop = 25;
                pdfPCell9.PaddingBottom = 50;
                table1.AddCell(pdfPCell9);

                document.Add(table);
                document.Add(tableProduct);
                document.Add(table1);

                document.Close();
                stream.Close();
                MessageBox.Show("In hóa đơn thành công!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                try
                {
                    System.Diagnostics.Process.Start("explorer.exe", savefile.FileName); // XML NHẬP
                }
                catch (Exception)
                {
                    System.Diagnostics.Process.Start("msedge.exe", savefile.FileName);
                }
            }
        }

        private void btnThoát_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}