//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BookManager.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BillDetail
    {
        public int TransactionID { get; set; }
        public int BookID { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> SalePrice { get; set; }
    
        public virtual Book Book { get; set; }
        public virtual BillOfSale BillOfSale { get; set; }
    }
}
