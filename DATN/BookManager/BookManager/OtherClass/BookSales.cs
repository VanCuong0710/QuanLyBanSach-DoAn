﻿namespace BookManager.OtherClass
{
    internal class BookSales
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string Category { get; set; }
        public int Quantity { get; set; }

        public int Price { get; set; }

        public int TotalPrice => Quantity * Price;

        public BookSales()
        {
        }
    }
}