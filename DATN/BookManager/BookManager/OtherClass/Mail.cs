﻿using BookManager.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace BookManager.OtherClass
{
    internal class Mail
    {
        private static ManagementBookEntities mydb = new ManagementBookEntities();

        /// <summary>
        /// Send email cảm ơn
        /// </summary>
        /// <param name="TranstactionID"></param>
        public static void SendMail(int TranstactionID)
        {
            var bill = mydb.BillOfSales.Find(TranstactionID);
            string from, pass, messageBody;
            MailMessage message = new MailMessage();

            string to = bill.Client.Email;
            from = "cuongcalmtodoit@gmail.com";
            pass = "iiluimgeoiinqxih";

            messageBody = Information(TranstactionID);
            message.Body = messageBody;

            message.To.Add(to);
            message.From = new MailAddress(from);

            message.Subject = "[MiniStop] Thư cảm ơn";
            SmtpClient smt = new SmtpClient("smtp.gmail.com");
            smt.EnableSsl = true;
            smt.Port = 587;
            smt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smt.Credentials = new NetworkCredential(from, pass);
            try
            {
                smt.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private static string Information(int TranstactionID)
        {
            var bill = mydb.BillOfSales.Find(TranstactionID);
            StringBuilder info = new StringBuilder();
            info.Append($"Cảm ơn bạn {bill.Client.ClientName} đã đến ghé thăm cửa hàng của chúng tôi ngày {bill.DateSale.ToString("dd/MM/yyyy")}. \n\n");
            int soluonghang = mydb.BillDetails.Count(p => p.TransactionID == TranstactionID);
            info.Append($"Bạn đã mua tổng cộng: {soluonghang}. Thông tin chi tiết về số lượng hàng đã mua là: \n");
            List<BookSales> list = new List<BookSales>();
            foreach (var item in mydb.BillDetails)
            {
                if (item.TransactionID == TranstactionID)
                {
                    BookSales bookSales = new BookSales();
                    bookSales.ID = item.Book.BookID;
                    bookSales.Category = item.Book.Category.CategoryName;
                    bookSales.Name = item.Book.BookName;
                    bookSales.Quantity = Convert.ToInt32(item.Quantity);
                    bookSales.Price = Convert.ToInt32(item.SalePrice);
                    list.Add(bookSales);
                }
            }
            foreach (var item in list)
            {
                info.Append($"Mã sách {item.ID} tên sách {item.Name} thể loại {item.Category} với số lượng {item.Quantity} giá là {item.Price.ToString("# ### ###")} VNĐ tổng sản phẩm này {item.TotalPrice.ToString("# ### ###")} VNĐ. \n");
            }
            var k = (from u in list
                     select u.TotalPrice).Sum();
            info.Append($"Tổng số lượng tiền: {k.ToString("# ### ###")} VNĐ\n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"Thông tin thêm: \n");
            int soluonghoadondamua = mydb.BillOfSales.Count(p => p.ClientID == bill.ClientID);
            info.Append($"Bạn đã mua tổng cộng {soluonghoadondamua} lần ở cửa hàng MiniStop. \n");

            var x = (from u in mydb.BillDetails
                     where u.BillOfSale.Client.ClientID == bill.ClientID
                     select u.Quantity * u.SalePrice).Sum();
            info.Append($"Bạn đã mua với số tiền : {x} ở cửa hàng MiniStop. \n\n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"MiniStop \n");
            info.Append($"Hà Nội \n");
            info.Append($"0866653231\n");
            return info.ToString();
        }

        public static void SendMail_ThanksYou(int ID)
        {
            var client = mydb.Clients.Find(ID);
            string from, pass, messageBody;
            MailMessage message = new MailMessage();

            string to = client.Email;
            from = "cuongcalmtodoit@gmail.com";
            pass = "iiluimgeoiinqxih";

            messageBody = Thank(ID);
            message.Body = messageBody;

            message.To.Add(to);
            message.From = new MailAddress(from);

            message.Subject = "[MiniStop] Thư mời tham gia sự kiện";
            SmtpClient smt = new SmtpClient("smtp.gmail.com");
            smt.EnableSsl = true;
            smt.Port = 587;
            smt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smt.Credentials = new NetworkCredential(from, pass);
            try
            {
                smt.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private static string Thank(int ID)
        {
            var Client = mydb.Clients.Find(ID);
            StringBuilder info = new StringBuilder();
            info.Append($"Cảm ơn bạn {Client.ClientName} đã mua hàng tại cửa hàng chúng tôi!!! \n");
            info.Append($"Bạn là một trong những khách hàng vip của tháng này. Bạn được mời tham phiên triển lãm trải nghiệm đọc sách tại cửa hàng chúng tôi miễn phí vào những hôm thứ 7 và chủ nhật!!!\n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"------------------------------------------------------ \n");
            info.Append($"MiniStop \n");
            info.Append($"Hà Nội \n");
            info.Append($"0866653231\n");
            return info.ToString();
        }
    }
}