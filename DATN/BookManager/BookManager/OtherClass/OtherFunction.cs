﻿using System;
using System.Windows.Forms;

namespace BookManager
{
    internal class OtherFunction
    {
        public static void SetDataGridView(DataGridView data)
        {
            data.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            data.DefaultCellStyle.ForeColor = System.Drawing.Color.BlueViolet;
            data.ColumnHeadersDefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            data.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("Times New Roman", 18, System.Drawing.FontStyle.Bold);
            data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.Red;
        }

        public static void SetFormatMoney(DataGridView data, int column)
        {
            data.Columns[column].DefaultCellStyle.Format = "# ### ### VNĐ";
        }

        public static void FormatNumber(DataGridView data, int column)
        {
            data.Columns[column].DefaultCellStyle.Format = "# ### ###";
        }

        public static void FormatTextbox(TextBox data)
        {
            int number = Convert.ToInt32(data.Text);
            data.Text = string.Format("{0:0,0}" + "VNĐ", number);
        }
    }
}