﻿using BookManager.Models;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace BookManager.Statistics
{
    public partial class BestSeller : Form
    {
        public BestSeller()
        {
            InitializeComponent();
        }

        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void BestSeller_Load(object sender, EventArgs e)
        {
            var totalSell = (from u in mydb.BillDetails
                             group u by u.BookID into results
                             select new
                             {
                                 BookID = results.Key,
                                 BookName = results.Select(b => b.Book.BookName).FirstOrDefault(),
                                 TotalSell = results.Sum(x => x.Quantity)
                             }).OrderByDescending(x => x.TotalSell).ToList();
            dgvHang.DataSource = totalSell;
            OtherFunction.SetDataGridView(dgvHang);
            OtherFunction.FormatNumber(dgvHang, 2);
            Chart.Series["ChartBDC"].Points.Clear();
            foreach (var item in totalSell)
            {
                Chart.Series["ChartBDC"].Points.AddXY($"{item.BookName}", item.TotalSell);

            }
            dgvHang.Columns[0].HeaderText = "Mã sách";
            dgvHang.Columns[1].HeaderText = "Tên sách";
            dgvHang.Columns[2].HeaderText = "Tổng số lượng bán ra";

        }

        private void btnLoc_Click(object sender, EventArgs e)
        {
            if (dtpStart.Value >= dtpFinish.Value)
                MessageBox.Show("Ngày bắt đầu không được lớn hơn hoặc bằng ngày kết thúc!!!");
            else
            {
                var totalSell = (from u in mydb.BillDetails
                                 where u.BillOfSale.DateSale >= dtpStart.Value && u.BillOfSale.DateSale <= dtpFinish.Value
                                 group u by u.BookID into results
                                 select new
                                 {
                                     BookID = results.Key,
                                     BookName = results.Select(b => b.Book.BookName).FirstOrDefault(),
                                     TotalSell = results.Sum(x => x.Quantity)
                                 }).OrderByDescending(x => x.TotalSell).ToList();
                dgvHang.DataSource = totalSell;
                OtherFunction.SetDataGridView(dgvHang);
                OtherFunction.FormatNumber(dgvHang, 2);
                Chart.Series["ChartBDC"].Points.Clear();
                int i = 0;
                foreach (var item in totalSell)
                {
                    Chart.Series["ChartBDC"].Points.Add((double)item.TotalSell);
                    Chart.Series["ChartBDC"].Points[i].Label = item.TotalSell.ToString();
                    Chart.Series["ChartBDC"].Points[i].Color = System.Drawing.Color.Blue;
                    Chart.Series["ChartBDC"].Points[i].AxisLabel = item.BookID.ToString();
                    i++;
                }
            }
        }
    }
}