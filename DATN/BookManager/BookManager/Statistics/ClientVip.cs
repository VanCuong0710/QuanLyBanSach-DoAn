﻿using BookManager.Models;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManager.Statistics
{
    public partial class ClientVip : Form
    {
        public ClientVip()
        {
            InitializeComponent();
        }

        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void ClientVip_Load(object sender, EventArgs e)
        {
            label1.Text = "Thống kê khách hàng vip tháng " + DateTime.Now.Month;
            var clients = (from c in mydb.Clients
                           group c by c.ClientID into results
                           select new
                           {
                               ClientID = results.Key,
                               ClientName = results.Select(c => c.ClientName).FirstOrDefault(),
                               ClientPhone = results.Select(c => c.PhoneNumber).FirstOrDefault(),
                               ClientAddress = results.Select(c => c.ClientAddress).FirstOrDefault(),
                               ClientEmail = results.Select(c => c.Email).FirstOrDefault(),
                               TotalMoney = results.Sum(b => b.BillOfSales.Sum(d => d.BillDetails.Sum(g => g.SalePrice * g.Quantity)))
                           }).OrderByDescending(c => c.TotalMoney);
            dataGridView1.DataSource = clients.ToList();
            foreach (var item in clients)
            {
                Chart.Series["ChartBDC"].Points.AddXY($"{item.ClientID}", item.TotalMoney);

            }
            OtherFunction.SetDataGridView(dataGridView1);
            OtherFunction.SetFormatMoney(dataGridView1, 5);
            dataGridView1.Columns[0].HeaderText = "Mã khách hàng";
            dataGridView1.Columns[1].HeaderText = "Tên khách hàng";
            dataGridView1.Columns[2].HeaderText = "Điện thoại khách hàng";
            dataGridView1.Columns[3].HeaderText = "Địa chỉ khách hàng";
            dataGridView1.Columns[4].HeaderText = "Email khách hàng";
            dataGridView1.Columns[5].HeaderText = "Tổng tiền chi";

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var clients = (from c in mydb.Clients
                           group c by c.ClientID into results
                           select new
                           {
                               ClientID = results.Key,
                               ClientName = results.Select(c => c.ClientName).FirstOrDefault(),
                               ClientPhone = results.Select(c => c.PhoneNumber).FirstOrDefault(),
                               ClientAddress = results.Select(c => c.ClientAddress).FirstOrDefault(),
                               ClientEmail = results.Select(c => c.Email).FirstOrDefault(),
                               TotalMoney = results.Sum(b => b.BillOfSales.Sum(d => d.BillDetails.Sum(g => g.SalePrice * g.Quantity)))
                           }).OrderByDescending(c => c.TotalMoney).ToList();
            if (clients.Count() >= 3)
            {
                for (int i = 1; i < 4; i++)
                {
                    await Task.Run(() => OtherClass.Mail.SendMail_ThanksYou(clients[i].ClientID));
                }
            }
            else

            {
                foreach (var item in clients)
                {
                    await Task.Run(() => OtherClass.Mail.SendMail_ThanksYou(item.ClientID));
                }
            }
            MessageBox.Show("Gửi mail thành công !!! ");
        }
    }
}