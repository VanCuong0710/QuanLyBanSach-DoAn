﻿using BookManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManager.Statistics
{
    public partial class KPI : Form
    {
        public KPI()
        {
            InitializeComponent();
        }
        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void KPI_Load(object sender, EventArgs e)
        {

        }

        private void btnLoc_Click(object sender, EventArgs e)
        {
            if (dtpStart.Value >= dtpFinish.Value)
                MessageBox.Show("Ngày bắt đầu không được lớn hơn hoặc bằng ngày kết thúc!!!");
            else
            {
                var list = (from u in mydb.BillOfSales
                           where u.DateSale >= dtpStart.Value && u.DateSale <= dtpFinish.Value
                           group u by u.AccountID into results
                           select new
                           {
                               AccountID = results.Key,
                               AccountName = results.Select(x => x.Account.Name).FirstOrDefault(),
                               TotalSum = results.Sum(x => x.BillDetails.Sum(z => z.SalePrice * z.Quantity))
                           }).OrderByDescending(x=>x.TotalSum).ToList();
                dgvNV.DataSource = list.ToList();
                Chart.Series["ChartBDC"].Points.Clear();
                foreach (var item in list)
                {
                    Chart.Series["ChartBDC"].Points.AddXY($"{item.AccountID}", item.TotalSum);

                }
                OtherFunction.SetDataGridView(dgvNV);
                OtherFunction.SetFormatMoney(dgvNV, 2);
                dgvNV.Columns[0].HeaderText = "Mã nhân viên";
                dgvNV.Columns[1].HeaderText = "Tên nhân viên";
                dgvNV.Columns[2].HeaderText = "Doanh số bán ra";

            }
        }
    }
}
