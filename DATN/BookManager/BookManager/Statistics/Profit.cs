﻿using BookManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManager.Statistics
{
    public partial class Profit : Form
    {
        public Profit()
        {
            InitializeComponent();
        }
        private ManagementBookEntities mydb = new ManagementBookEntities();
        private void Profit_Load(object sender, EventArgs e)
        {
            var listXuats = from u in mydb.BillOfSales
                            group u by u.TransactionID into results
                            select new
                            {
                                TransactionID = results.Key,
                                AccountName = results.Select(x => x.Account.Name).FirstOrDefault(),
                                ClientName=results.Select(x=>x.Client.ClientName).FirstOrDefault(),
                                Date= results.Select(x => x.DateSale).FirstOrDefault(),
                                TotalMoney = results.Sum(x => x.BillDetails.Sum(z =>(z.Quantity)*(z.SalePrice)))
                            };
            dgvXuat.DataSource=listXuats.ToList();
            OtherFunction.SetDataGridView(dgvXuat);
            dgvXuat.Columns[0].HeaderText = "Mã giao dịch";
            dgvXuat.Columns[1].HeaderText = "Tên người lập";
            dgvXuat.Columns[2].HeaderText = "Tên khách hàng";
            dgvXuat.Columns[3].HeaderText = "Ngày tạo";
            dgvXuat.Columns[4].HeaderText = "Tổng tiền";
            var listNhaps = from u in mydb.BillImports
                            group u by u.TransactionID into results
                            select new
                            {
                                TransactionID = results.Key,
                                AccountName = results.Select(x => x.Account.Name).FirstOrDefault(),
                                SupplierName = results.Select(x => x.Supplier.SupplierName).FirstOrDefault(),
                                Date = results.Select(x => x.CreatedDate).FirstOrDefault(),
                                TotalMoney = results.Sum(x => x.ImportBillDetails.Sum(z => (z.Quantity) * (z.ImportPrice)))
                            };
            dgvNhap.DataSource = listNhaps.ToList();
            OtherFunction.SetDataGridView(dgvNhap);
            OtherFunction.SetFormatMoney(dgvXuat, 4);
            OtherFunction.SetFormatMoney(dgvNhap, 4);
            tinhTongTienXuat();
            tinhTongTienNhap();
            Chart();
            dgvNhap.Columns[0].HeaderText = "Mã giao dịch";
            dgvNhap.Columns[1].HeaderText = "Tên người lập";
            dgvNhap.Columns[2].HeaderText = "Nhà cung cấp";
            dgvNhap.Columns[3].HeaderText = "Ngày tạo";
            dgvNhap.Columns[4].HeaderText = "Tổng tiền";

        }
        private void tinhTongTienXuat()
        {
            int tongtien = 0;
            for (int i = 0; i < dgvXuat.RowCount; i++)
            {
                string tien = dgvXuat.Rows[i].Cells[4].Value.ToString();
                tien = tien.Replace("VNĐ", "");
                tien = tien.Replace(" ", "");
                tongtien = tongtien + Convert.ToInt32(tien);
            }
            txtTongThu.Text = tongtien.ToString();
            OtherFunction.FormatTextbox(txtTongThu);
        }
        private void tinhTongTienNhap()
        {
            int tongtien = 0;
            for (int i = 0; i < dgvNhap.RowCount; i++)
            {
                string tien = dgvNhap.Rows[i].Cells[4].Value.ToString();
                tien = tien.Replace("VNĐ", "");
                tien = tien.Replace(" ", "");
                tongtien = tongtien + Convert.ToInt32(tien);
            }
            txtTongChi.Text = tongtien.ToString();
            OtherFunction.FormatTextbox(txtTongChi);
        }
        private void Chart()
        {
            int tongtienThu = 0;
            for (int i = 0; i < dgvXuat.RowCount; i++)
            {
                string tien = dgvXuat.Rows[i].Cells[4].Value.ToString();
                tien = tien.Replace("VNĐ", "");
                tien = tien.Replace(" ", "");
                tongtienThu = tongtienThu + Convert.ToInt32(tien);
            }

            int tongtienChi = 0;
            for (int i = 0; i < dgvNhap.RowCount; i++)
            {
                string tien = dgvNhap.Rows[i].Cells[4].Value.ToString();
                tien = tien.Replace("VNĐ", "");
                tien = tien.Replace(" ", "");
                tongtienChi = tongtienChi + Convert.ToInt32(tien);
            }
            textBox1.Text=(-tongtienChi+tongtienThu).ToString();
            OtherFunction.FormatTextbox(textBox1);
            List<TongThu> tongThus = new List<TongThu>();
            tongThus.Add(new TongThu() { Name = "Tổng chi", Number = tongtienChi });
            tongThus.Add(new TongThu() { Name = "Tổng thu", Number = tongtienThu });
            ChartBDT.DataSource = tongThus;
            ChartBDT.Series["ChartBDC"].XValueMember = "Name";
            ChartBDT.Series["ChartBDC"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            ChartBDT.Series["ChartBDC"].YValueMembers = "Number";
            ChartBDT.Series["ChartBDC"].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;

        }
        class TongThu
        {
            public string Name { get; set; }
            public int Number { get; set; }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dtpStart.Value >= dtpFinish.Value)
                MessageBox.Show("Ngày bắt đầu không được lớn hơn hoặc bằng ngày kết thúc!!!");
            else
            {
                var listXuats = from u in mydb.BillOfSales
                                where u.DateSale>=dtpStart.Value && u.DateSale<=dtpFinish.Value
                                group u by u.TransactionID into results
                                select new
                                {
                                    TransactionID = results.Key,
                                    AccountName = results.Select(x => x.Account.Name).FirstOrDefault(),
                                    ClientName = results.Select(x => x.Client.ClientName).FirstOrDefault(),
                                    Date = results.Select(x => x.DateSale).FirstOrDefault(),
                                    TotalMoney = results.Sum(x => x.BillDetails.Sum(z => (z.Quantity) * (z.SalePrice)))
                                };
                dgvXuat.DataSource = listXuats.ToList();
                OtherFunction.SetDataGridView(dgvXuat);

                var listNhaps = from u in mydb.BillImports
                                where u.CreatedDate >= dtpStart.Value && u.CreatedDate <= dtpFinish.Value
                                group u by u.TransactionID into results
                                select new
                                {
                                    TransactionID = results.Key,
                                    AccountName = results.Select(x => x.Account.Name).FirstOrDefault(),
                                    SupplierName = results.Select(x => x.Supplier.SupplierName).FirstOrDefault(),
                                    Date = results.Select(x => x.CreatedDate).FirstOrDefault(),
                                    TotalMoney = results.Sum(x => x.ImportBillDetails.Sum(z => (z.Quantity) * (z.ImportPrice)))
                                };
                dgvNhap.DataSource = listNhaps.ToList();
                OtherFunction.SetDataGridView(dgvNhap);
                OtherFunction.SetFormatMoney(dgvXuat, 4);
                OtherFunction.SetFormatMoney(dgvNhap, 4);
                tinhTongTienXuat();
                tinhTongTienNhap();
                Chart();
            }    
        }
    }
}
