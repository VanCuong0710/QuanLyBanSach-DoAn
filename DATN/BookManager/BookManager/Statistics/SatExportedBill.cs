﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BookManager.DetailBill;
using BookManager.Models;

namespace BookManager.UI
{
    public partial class SatExportedBill : Form
    {
        public SatExportedBill()
        {
            InitializeComponent();
        }
        private ManagementBookEntities mydb = new ManagementBookEntities();
        private void cbbTieuChi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbTieuChi.Text == "Theo mã giao dịch")
            {
                groupTimTheoID.Visible = true;
                grTheoNgay.Visible = false;
                grTheonguoilap.Visible = false;
                grTheonhaxuatban.Visible = false;
            }
            if (cbbTieuChi.Text == "Theo người lập")
            {
                grTheonguoilap.Visible = true;
                groupTimTheoID.Visible = false;
                grTheoNgay.Visible = false;
                grTheonhaxuatban.Visible = false;
            }
            if (cbbTieuChi.Text == "Theo người mua")
            {
                grTheonhaxuatban.Visible = true;
                groupTimTheoID.Visible = false;
                grTheoNgay.Visible = false;
                grTheonguoilap.Visible = false;
            }
            if (cbbTieuChi.Text == "Theo ngày")
            {
                grTheoNgay.Visible = true;
                groupTimTheoID.Visible = false;
                grTheonguoilap.Visible = false;
                grTheonhaxuatban.Visible = false;
            }
        }

        private void SatExportedBill_Load(object sender, EventArgs e)
        {
            var listBills = from u in mydb.BillOfSales
                            select new
                            {
                                u.TransactionID,
                                u.AccountID,
                                u.ClientID,
                                u.DateSale
                            };
            dgvBill.DataSource = listBills.ToList();
            dgvBill.Columns[0].HeaderText = "Mã giao dịch";
            dgvBill.Columns[1].HeaderText = "Mã người tạo";
            dgvBill.Columns[2].HeaderText = "Mã khách hàng mua";
            dgvBill.Columns[3].HeaderText = "Ngày bán";
            cbbTieuChi.SelectedIndex = 0;
            OtherFunction.SetDataGridView(dgvBill);
            var nguoiLaps = from u in mydb.Accounts
                            select u;
            cbbNguoiLap.DataSource = nguoiLaps.ToList();
            cbbNguoiLap.DisplayMember = "Name";
            cbbNguoiLap.ValueMember = "AccountID";


            var pubs = from u in mydb.Clients
                       select u;
            cbbNxb.DataSource = pubs.ToList();
            cbbNxb.DisplayMember = "ClientName"; 
            cbbNxb.ValueMember = "ClientID";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cbbTieuChi.Text == "Theo mã giao dịch")
            {
                int.TryParse(txtTransactionID.Text, out int ID);
                var listBills = from u in mydb.BillOfSales
                                where u.TransactionID == ID
                                select new
                                {
                                    u.TransactionID,
                                    u.AccountID,
                                    u.ClientID,
                                    u.DateSale
                                };
                dgvBill.DataSource = listBills.ToList();
            }
            if (cbbTieuChi.Text == "Theo người lập")
            {
                int.TryParse(cbbNguoiLap.SelectedValue.ToString(), out int ID);
                var listBills = from u in mydb.BillOfSales
                                where u.AccountID == ID
                                select new
                                {
                                    u.TransactionID,
                                    u.AccountID,
                                    u.ClientID,
                                    u.DateSale
                                };
                dgvBill.DataSource = listBills.ToList();
            }
            if (cbbTieuChi.Text == "Theo người mua")
            {
                int.TryParse(cbbNxb.SelectedValue.ToString(), out int ID);
                var listBills = from u in mydb.BillOfSales
                                where u.ClientID == ID
                                select new
                                {
                                    u.TransactionID,
                                    u.AccountID,
                                    u.ClientID,
                                    u.DateSale
                                };
                dgvBill.DataSource = listBills.ToList();
            }
            if (cbbTieuChi.Text == "Theo ngày")
            {
                if (dtpFinish.Value <= dtpStart.Value)
                {
                    MessageBox.Show("Ngày kết thúc phải lớn hơn ngày sau!!!");
                }
                else
                {
                    var listBills = from u in mydb.BillOfSales
                                    where u.DateSale >= dtpStart.Value && u.DateSale <= dtpFinish.Value
                                    select new
                                    {
                                        u.TransactionID,
                                        u.AccountID,
                                        u.ClientID,
                                        u.DateSale
                                    };
                    dgvBill.DataSource = listBills.ToList();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SatExportedBill_Load(sender, e);
        }

        private void dgvBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtTranID.Text = dgvBill.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
            catch (Exception)
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtTranID.Text == "")
                MessageBox.Show("Bạn chưa chọn hóa đơn để xem!!!");
            else
            {
                DetailExportedBill detail = new DetailExportedBill();
                detail.TransactionID = Convert.ToInt32(txtTranID.Text);
                detail.Show();
            }    
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
