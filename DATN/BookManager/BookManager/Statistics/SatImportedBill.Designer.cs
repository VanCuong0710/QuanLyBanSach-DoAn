﻿namespace BookManager.UI
{
    partial class SatImportedBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvBill = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTranID = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.grTheoNgay = new System.Windows.Forms.GroupBox();
            this.dtpFinish = new System.Windows.Forms.DateTimePicker();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.grTheonhaxuatban = new System.Windows.Forms.GroupBox();
            this.cbbNxb = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.groupTimTheoID = new System.Windows.Forms.GroupBox();
            this.txtTransactionID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grTheonguoilap = new System.Windows.Forms.GroupBox();
            this.cbbNguoiLap = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbbTieuChi = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBill)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.grTheoNgay.SuspendLayout();
            this.grTheonhaxuatban.SuspendLayout();
            this.groupTimTheoID.SuspendLayout();
            this.grTheonguoilap.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvBill);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 515);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1347, 315);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hóa đơn đã nhập";
            // 
            // dgvBill
            // 
            this.dgvBill.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBill.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvBill.Location = new System.Drawing.Point(3, 45);
            this.dgvBill.Name = "dgvBill";
            this.dgvBill.RowTemplate.Height = 24;
            this.dgvBill.Size = new System.Drawing.Size(1341, 267);
            this.dgvBill.TabIndex = 0;
            this.dgvBill.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBill_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtTranID);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.grTheoNgay);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.grTheonhaxuatban);
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Controls.Add(this.groupTimTheoID);
            this.groupBox2.Controls.Add(this.grTheonguoilap);
            this.groupBox2.Controls.Add(this.cbbTieuChi);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(71, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1218, 476);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lọc hóa dơn";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(190, 421);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 21);
            this.label7.TabIndex = 11;
            this.label7.Text = "Mã giao dịch";
            // 
            // txtTranID
            // 
            this.txtTranID.Enabled = false;
            this.txtTranID.Location = new System.Drawing.Point(312, 418);
            this.txtTranID.Name = "txtTranID";
            this.txtTranID.Size = new System.Drawing.Size(223, 29);
            this.txtTranID.TabIndex = 10;
            // 
            // button2
            // 
            this.button2.Image = global::BookManager.Properties.Resources.XemChiTiet_Format;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(609, 394);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(260, 75);
            this.button2.TabIndex = 7;
            this.button2.Text = "Xem chi tiết";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // grTheoNgay
            // 
            this.grTheoNgay.Controls.Add(this.dtpFinish);
            this.grTheoNgay.Controls.Add(this.dtpStart);
            this.grTheoNgay.Controls.Add(this.label6);
            this.grTheoNgay.Controls.Add(this.label5);
            this.grTheoNgay.Location = new System.Drawing.Point(441, 215);
            this.grTheoNgay.Name = "grTheoNgay";
            this.grTheoNgay.Size = new System.Drawing.Size(391, 153);
            this.grTheoNgay.TabIndex = 6;
            this.grTheoNgay.TabStop = false;
            this.grTheoNgay.Text = "Lọc theo ngày";
            this.grTheoNgay.Visible = false;
            this.grTheoNgay.Enter += new System.EventHandler(this.grTheoNgay_Enter);
            // 
            // dtpFinish
            // 
            this.dtpFinish.CustomFormat = "dd/MM/yyyy";
            this.dtpFinish.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFinish.Location = new System.Drawing.Point(145, 90);
            this.dtpFinish.Name = "dtpFinish";
            this.dtpFinish.Size = new System.Drawing.Size(183, 29);
            this.dtpFinish.TabIndex = 3;
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "dd/MM/yyyy";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(145, 45);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(183, 29);
            this.dtpStart.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 21);
            this.label6.TabIndex = 1;
            this.label6.Text = "Ngày kết thúc";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ngày bắt đầu";
            // 
            // button1
            // 
            this.button1.Image = global::BookManager.Properties.Resources.Refesh_Format;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(877, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(260, 75);
            this.button1.TabIndex = 3;
            this.button1.Text = "Làm mới";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // grTheonhaxuatban
            // 
            this.grTheonhaxuatban.Controls.Add(this.cbbNxb);
            this.grTheonhaxuatban.Controls.Add(this.label4);
            this.grTheonhaxuatban.Location = new System.Drawing.Point(30, 215);
            this.grTheonhaxuatban.Name = "grTheonhaxuatban";
            this.grTheonhaxuatban.Size = new System.Drawing.Size(391, 100);
            this.grTheonhaxuatban.TabIndex = 5;
            this.grTheonhaxuatban.TabStop = false;
            this.grTheonhaxuatban.Text = "Lọc theo nhà xuất bản";
            // 
            // cbbNxb
            // 
            this.cbbNxb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbNxb.FormattingEnabled = true;
            this.cbbNxb.Location = new System.Drawing.Point(119, 41);
            this.cbbNxb.Name = "cbbNxb";
            this.cbbNxb.Size = new System.Drawing.Size(255, 29);
            this.cbbNxb.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nhà cung cấp";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::BookManager.Properties.Resources.Thongke_Format;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(877, 67);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(260, 75);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // groupTimTheoID
            // 
            this.groupTimTheoID.Controls.Add(this.txtTransactionID);
            this.groupTimTheoID.Controls.Add(this.label2);
            this.groupTimTheoID.Location = new System.Drawing.Point(441, 97);
            this.groupTimTheoID.Name = "groupTimTheoID";
            this.groupTimTheoID.Size = new System.Drawing.Size(391, 100);
            this.groupTimTheoID.TabIndex = 3;
            this.groupTimTheoID.TabStop = false;
            this.groupTimTheoID.Text = "Lọc theo mã giao dịch";
            this.groupTimTheoID.Visible = false;
            // 
            // txtTransactionID
            // 
            this.txtTransactionID.Location = new System.Drawing.Point(119, 42);
            this.txtTransactionID.Name = "txtTransactionID";
            this.txtTransactionID.Size = new System.Drawing.Size(255, 29);
            this.txtTransactionID.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã giao dịch";
            // 
            // grTheonguoilap
            // 
            this.grTheonguoilap.Controls.Add(this.cbbNguoiLap);
            this.grTheonguoilap.Controls.Add(this.label3);
            this.grTheonguoilap.Location = new System.Drawing.Point(30, 97);
            this.grTheonguoilap.Name = "grTheonguoilap";
            this.grTheonguoilap.Size = new System.Drawing.Size(391, 100);
            this.grTheonguoilap.TabIndex = 4;
            this.grTheonguoilap.TabStop = false;
            this.grTheonguoilap.Text = "Lọc theo người lập";
            this.grTheonguoilap.Visible = false;
            // 
            // cbbNguoiLap
            // 
            this.cbbNguoiLap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbNguoiLap.FormattingEnabled = true;
            this.cbbNguoiLap.Location = new System.Drawing.Point(119, 41);
            this.cbbNguoiLap.Name = "cbbNguoiLap";
            this.cbbNguoiLap.Size = new System.Drawing.Size(255, 29);
            this.cbbNguoiLap.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "Người lập";
            // 
            // cbbTieuChi
            // 
            this.cbbTieuChi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbTieuChi.FormattingEnabled = true;
            this.cbbTieuChi.Items.AddRange(new object[] {
            "Theo mã giao dịch",
            "Theo người lập",
            "Theo nhà cung cấp",
            "Theo ngày"});
            this.cbbTieuChi.Location = new System.Drawing.Point(123, 47);
            this.cbbTieuChi.Name = "cbbTieuChi";
            this.cbbTieuChi.Size = new System.Drawing.Size(412, 29);
            this.cbbTieuChi.TabIndex = 1;
            this.cbbTieuChi.SelectedIndexChanged += new System.EventHandler(this.cbbTieuChi_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tiêu chí";
            // 
            // SatImportedBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1347, 830);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Times New Roman", 14.25F);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "SatImportedBill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SatImportedBill";
            this.Load += new System.EventHandler(this.SatImportedBill_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBill)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grTheoNgay.ResumeLayout(false);
            this.grTheoNgay.PerformLayout();
            this.grTheonhaxuatban.ResumeLayout(false);
            this.grTheonhaxuatban.PerformLayout();
            this.groupTimTheoID.ResumeLayout(false);
            this.groupTimTheoID.PerformLayout();
            this.grTheonguoilap.ResumeLayout(false);
            this.grTheonguoilap.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvBill;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbbTieuChi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox groupTimTheoID;
        private System.Windows.Forms.TextBox txtTransactionID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox grTheonguoilap;
        private System.Windows.Forms.ComboBox cbbNguoiLap;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grTheonhaxuatban;
        private System.Windows.Forms.ComboBox cbbNxb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox grTheoNgay;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpFinish;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTranID;
    }
}