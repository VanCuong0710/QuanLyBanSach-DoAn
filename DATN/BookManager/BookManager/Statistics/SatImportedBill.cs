﻿using BookManager.DetailBill;
using BookManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class SatImportedBill : Form
    {
        public SatImportedBill()
        {
            InitializeComponent();
        }
        private ManagementBookEntities mydb = new ManagementBookEntities();
        private void SatImportedBill_Load(object sender, EventArgs e)
        {
            var listBills = from u in mydb.BillImports
                            select new
                            {
                                u.TransactionID,
                                u.AccountID,
                                u.PublisherID,
                                u.CreatedDate
                            };
            dgvBill.DataSource = listBills.ToList();
            cbbTieuChi.SelectedIndex = 0;
            OtherFunction.SetDataGridView(dgvBill);
            var nguoiLaps = from u in mydb.Accounts
                            select u;
            cbbNguoiLap.DataSource = nguoiLaps.ToList();
            cbbNguoiLap.DisplayMember = "Name";
            cbbNguoiLap.ValueMember = "AccountID";


            var pubs = from u in mydb.Suppliers
                       select u;
            cbbNxb.DataSource = pubs.ToList();
            dgvBill.DataSource = listBills.ToList();
            dgvBill.Columns[0].HeaderText = "Mã giao dịch";
            dgvBill.Columns[1].HeaderText = "Mã người tạo";
            dgvBill.Columns[2].HeaderText = "Mã nhà cung cấp";
            dgvBill.Columns[3].HeaderText = "Ngày tạo";
            cbbNxb.DisplayMember = "SupplierName";
            cbbNxb.ValueMember = "SupplierID";
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cbbTieuChi.Text == "Theo mã giao dịch")
            {
                int.TryParse(txtTransactionID.Text, out int ID);
                var listBills = from u in mydb.BillImports
                                where u.TransactionID == ID
                                select new
                                {
                                    u.TransactionID,
                                    u.AccountID,
                                    u.PublisherID,
                                    u.CreatedDate
                                };
                dgvBill.DataSource = listBills.ToList();
            }
            if (cbbTieuChi.Text == "Theo người lập")
            {
                int.TryParse(cbbNguoiLap.SelectedValue.ToString(), out int ID);
                var listBills = from u in mydb.BillImports
                                where u.AccountID == ID
                                select new
                                {
                                    u.TransactionID,
                                    u.AccountID,
                                    u.PublisherID,
                                    u.CreatedDate
                                };
                dgvBill.DataSource = listBills.ToList();
            }
            if (cbbTieuChi.Text == "Theo nhà cung cấp")
            {
                int.TryParse(cbbNxb.SelectedValue.ToString(), out int ID);
                var listBills = from u in mydb.BillImports
                                where u.PublisherID == ID
                                select new
                                {
                                    u.TransactionID,
                                    u.AccountID,
                                    u.PublisherID,
                                    u.CreatedDate
                                };
                dgvBill.DataSource = listBills.ToList();
            }
            if (cbbTieuChi.Text == "Theo ngày")
            {
                if (dtpFinish.Value <= dtpStart.Value)
                {
                    MessageBox.Show("Ngày kết thúc phải lớn hơn ngày sau!!!");
                }
                else
                {
                    var listBills = from u in mydb.BillImports
                                    where u.CreatedDate >= dtpStart.Value && u.CreatedDate <= dtpFinish.Value
                                    select new
                                    {
                                        u.TransactionID,
                                        u.AccountID,
                                        u.PublisherID,
                                        u.CreatedDate
                                    };
                    dgvBill.DataSource = listBills.ToList();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SatImportedBill_Load(sender, e);
        }

        private void grTheoNgay_Enter(object sender, EventArgs e)
        {

        }

        private void cbbTieuChi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbTieuChi.Text == "Theo mã giao dịch")
            {
                groupTimTheoID.Visible = true;
                grTheoNgay.Visible = false;
                grTheonguoilap.Visible = false;
                grTheonhaxuatban.Visible = false;
            }
            if (cbbTieuChi.Text == "Theo người lập")
            {
                grTheonguoilap.Visible = true;
                groupTimTheoID.Visible = false;
                grTheoNgay.Visible = false;
                grTheonhaxuatban.Visible = false;
            }
            if (cbbTieuChi.Text == "Theo nhà cung cấp")
            {
                grTheonhaxuatban.Visible = true;
                groupTimTheoID.Visible = false;
                grTheoNgay.Visible = false;
                grTheonguoilap.Visible = false;
            }
            if (cbbTieuChi.Text == "Theo ngày")
            {
                grTheoNgay.Visible = true;
                groupTimTheoID.Visible = false;
                grTheonguoilap.Visible = false;
                grTheonhaxuatban.Visible = false;
            }
        }

       

        private void dgvBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtTranID.Text = dgvBill.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
            catch (Exception)
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtTranID.Text))
            {
                MessageBox.Show("Chưa có hóa đơn nhập để xem!!!");
            }    
            else
            {
                DetailImportedBill detail = new DetailImportedBill();
                detail.TransactionID=Convert.ToInt32(txtTranID.Text);
                detail.Show();
            }    
        }
    }
}
