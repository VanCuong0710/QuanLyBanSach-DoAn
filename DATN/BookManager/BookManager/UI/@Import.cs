﻿using BookManager.DetailBill;
using BookManager.Models;
using BookManager.OtherClass;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class _Import : Form
    {
        public int ID { get; set; } = 1;
        private int tongtien = 0;
        private ManagementBookEntities mydb = new ManagementBookEntities();

        public _Import()
        {
            InitializeComponent();
        }

        private void _Import_Load(object sender, EventArgs e)
        {
            txtMhd.Text = (mydb.BillImports.Count() + 1).ToString();
            txtNguoiLap.Text = (mydb.Accounts.Find(ID)).Name;
            var listHangs = from u in mydb.Books
                            where u.Status == 0
                            select u;
            cbbHang.DataSource = listHangs.ToList();
            cbbHang.DisplayMember = "BookName";
            cbbHang.ValueMember = "BookID";
            if (int.TryParse(cbbHang.SelectedValue.ToString(), out int id) == true)
            {
                var book = mydb.Books.Find(id);
                txtTheLoai.Text = book.Category.CategoryName;
                txtGia.Text = ((int)(book.Price * 0.95)).ToString();
            }

            var listPublisher = from u in mydb.Suppliers
                                where u.Status == 0
                                select u;
            cbbPuslisher.DataSource = listPublisher.ToList();
            cbbPuslisher.DisplayMember = "SupplierName";
            cbbPuslisher.ValueMember = "SupplierID";
            if (int.TryParse(cbbPuslisher.SelectedValue.ToString(), out int id1) == true)
            {
                var publisher = mydb.Suppliers.Find(id1);
                txtSDT.Text = publisher.SupplierPhone.ToString();
                txtDc.Text = publisher.SupplierAddress.ToString();
            }


            OtherFunction.SetDataGridView(dgvDshang);
            OtherFunction.SetFormatMoney(dgvDshang, 4);
            OtherFunction.SetFormatMoney(dgvDshang, 5);
            tinhTongTien();
        }

        private void cbbHang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.TryParse(cbbHang.SelectedValue.ToString(), out int id) == true)
            {
                var book = mydb.Books.Find(id);
                txtTheLoai.Text = book.Category.CategoryName;
                txtGia.Text = ((int)(book.Price * 0.95)).ToString();
            }
        }

        private void txtSoLuongBan_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void cbbPuslisher_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.TryParse(cbbPuslisher.SelectedValue.ToString(), out int id) == true)
            {
                var publisher = mydb.Suppliers.Find(id);
                txtSDT.Text = publisher.SupplierPhone.ToString();
                txtDc.Text = publisher.SupplierAddress.ToString();
            }
        }
        private void tinhTongTien()
        {
            tongtien = 0;
            for (int i = 0; i < dgvDshang.RowCount; i++)
            {
                string tien = dgvDshang.Rows[i].Cells[5].Value.ToString();
                tien = tien.Replace("VNĐ", "");
                tien = tien.Replace(" ", "");
                tongtien = tongtien + Convert.ToInt32(tien);
            }
            txtTongTien.Text = tongtien.ToString();
            OtherFunction.FormatTextbox(txtTongTien);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSoLuongBan.Text))
            {
                MessageBox.Show("Không được để trắng số lượng bán");
            }
            else if (string.IsNullOrWhiteSpace(txtGia.Text))
            {
                MessageBox.Show("Giá nhập không được để trống");
            }
            else if (Convert.ToInt32(txtGia.Text) <= 0)
            {
                MessageBox.Show("Giá nhập phải lớn hơn 0");
            }
            else if(Convert.ToInt32(txtGia.Text)>mydb.Books.Find(Convert.ToInt32(cbbHang.SelectedValue)).Price)

            {
                MessageBox.Show($"Giá nhập phải nhỏ hơn giá bán [Giá hiện tại {mydb.Books.Find(Convert.ToInt32(cbbHang.SelectedValue)).Price}]");
            }        
            else
            {
                bool checkHang = true;
                BookSales bookSales = new BookSales();
                bookSales.ID = Convert.ToInt32(cbbHang.SelectedValue);
                bookSales.Name = cbbHang.Text;
                bookSales.Price = Convert.ToInt32(txtGia.Text);
                bookSales.Category = txtTheLoai.Text;
                bookSales.Quantity = Convert.ToInt32(txtSoLuongBan.Text);
                for (int i = 0; i < dgvDshang.RowCount; i++)
                {
                    if (bookSales.ID == Convert.ToInt32(dgvDshang.Rows[i].Cells[0].Value.ToString()))
                    {
                        checkHang = false;
                        break;
                    }
                }
                if (checkHang == true)
                {
                    dgvDshang.Rows.Add(bookSales.ID, bookSales.Name, bookSales.Category, bookSales.Quantity, bookSales.Price, bookSales.TotalPrice);
                }
                else
                {
                    MessageBox.Show("Hàng đã tồn tại không thể thêm");
                }
            }
            tinhTongTien();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void txtGia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            int? index = null;
            for (int i = 0; i < dgvDshang.RowCount; i++)
            {
                if (cbbHang.SelectedValue.ToString() == dgvDshang.Rows[i].Cells[0].Value.ToString())
                {
                    index = i;
                    break;
                }
            }
            if (index == null)
            {
                MessageBox.Show("Vui lòng chọn hàng để xóa");
            }
            else
            {
                dgvDshang.Rows.RemoveAt(Convert.ToInt32(index));
            }
            tinhTongTien();
        }

        private void dgvDshang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                cbbHang.Text = dgvDshang.Rows[index].Cells[1].Value.ToString();
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            if (dgvDshang.RowCount == 0)
            {
                MessageBox.Show("Danh sách hàng chưa có sản phẩm nào!!!");
            }
            else
            {
                DialogResult kq = MessageBox.Show("Vui lòng kiểm tra kỹ thông tin", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (kq == DialogResult.Yes)
                {
                    BillImport billImport = new BillImport();
                    int.TryParse(cbbPuslisher.SelectedValue.ToString(), out int id);
                    billImport.PublisherID = id;
                    billImport.AccountID = ID;
                    billImport.CreatedDate = dtpSale.Value;
                    mydb.BillImports.Add(billImport);
                    mydb.SaveChanges();

                    for (int i = 0; i < dgvDshang.Rows.Count; i++)
                    {
                        ImportBillDetail billDetail = new ImportBillDetail();
                        billDetail.TransactionID = billImport.TransactionID;
                        billDetail.BookID = Convert.ToInt32(dgvDshang.Rows[i].Cells[0].Value.ToString());
                        billDetail.Quantity = Convert.ToInt32(dgvDshang.Rows[i].Cells[3].Value.ToString());
                        billDetail.ImportPrice = Convert.ToInt32(dgvDshang.Rows[i].Cells[4].Value.ToString());
                        var book = mydb.Books.Find(billDetail.BookID);
                        book.Quantity = Convert.ToInt32(book.Quantity + billDetail.Quantity);
                        mydb.ImportBillDetails.Add(billDetail);
                        mydb.SaveChanges();
                    }
                    MessageBox.Show("Tạo hóa đơn thành công!!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    DetailImportedBill detail = new DetailImportedBill();
                    detail.TransactionID = mydb.BillImports.Count();
                    detail.Show();
                }
            }


        }
    }
}
