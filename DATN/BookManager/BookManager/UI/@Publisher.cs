﻿using BookManager.Models;
using ExcelDataReader;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using app = Microsoft.Office.Interop.Excel.Application;

namespace BookManager.UI
{
    public partial class @Publisher : Form
    {
        public @Publisher()
        {
            InitializeComponent();
        }

        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void Publisher_Load(object sender, EventArgs e)
        {
            var publishers = (from p in mydb.Suppliers
                              select new
                              {
                                  ID = p.SupplierID,
                                  Name = p.SupplierName,
                                  Phone = p.SupplierPhone,
                                  Address = p.SupplierAddress,
                                  Email = p.Email,
                                  Status = p.Status,
                              }
                            ).ToList();
            dgvPublisher.DataSource = publishers;
            OtherFunction.SetDataGridView(dgvPublisher);
            cbbStatus.SelectedIndex = 0;
            dgvPublisher.Columns[0].HeaderText = "Mã nhà cung cấp";
            dgvPublisher.Columns[1].HeaderText = "Tên nhà cung cấp";
            dgvPublisher.Columns[2].HeaderText = "Điện thoại cung cấp";
            dgvPublisher.Columns[3].HeaderText = "Địa chỉ cung cấp";
            dgvPublisher.Columns[4].HeaderText = "Email nhà cung cấp";
            dgvPublisher.Columns[5].HeaderText = "Trạng thái";

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtID.Clear();
            txtAddress.Clear();
            txtEmail.Clear();
            txtName.Clear();
            txtPhone.Clear();
            txtID.Enabled = true;
            cbbStatus.SelectedIndex = 0;
            txtSearch.Clear();
            Publisher_Load(sender, e);
        }

        private void dgvPublisher_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                txtID.Text = dgvPublisher.Rows[index].Cells[0].Value.ToString();
                txtName.Text = dgvPublisher.Rows[index].Cells[1].Value.ToString();
                txtPhone.Text = dgvPublisher.Rows[index].Cells[2].Value.ToString();
                txtAddress.Text = dgvPublisher.Rows[index].Cells[3].Value.ToString();
                txtEmail.Text = dgvPublisher.Rows[index].Cells[4].Value.ToString();
                if (dgvPublisher.Rows[index].Cells[5].Value.ToString() == "0")
                    cbbStatus.SelectedIndex = 0;
                else
                    cbbStatus.SelectedIndex = 1;
                txtID.Enabled = false;
            }
            catch (Exception) { }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            var publishers = (from p in mydb.Suppliers
                              where p.SupplierName.ToUpper().Contains(txtSearch.Text.ToUpper())
                              select new
                              {
                                  ID = p.SupplierID,
                                  Name = p.SupplierName,
                                  Phone = p.SupplierPhone,
                                  Address = p.SupplierAddress,
                                  Email = p.Email,
                                  Status = p.Status,
                              }
                           ).ToList();
            dgvPublisher.DataSource = publishers;
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPhone.Text) || string.IsNullOrWhiteSpace(txtAddress.Text)
                || string.IsNullOrWhiteSpace(txtName.Text) || string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                MessageBox.Show("Không được để trắng dữ liệu!!!");
            }
            else if (txtPhone.Text.Length != 10)
                MessageBox.Show("Điện thoại phải có 10 số!!!");
            else
            {
                var check = mydb.Suppliers.FirstOrDefault(p => p.SupplierName == txtName.Text);
                if (check != null)
                    MessageBox.Show("Nhà xuất bản này đã tồn tại");
                else
                {
                    Models.Supplier publishers = new Models.Supplier();
                    publishers.SupplierPhone = txtPhone.Text;
                    publishers.SupplierName = txtName.Text;
                    publishers.SupplierAddress = txtAddress.Text;
                    publishers.Email = txtEmail.Text;
                    if (cbbStatus.SelectedIndex == 0)
                        publishers.Status = 0;
                    else
                        publishers.Status = 1;
                    mydb.Suppliers.Add(publishers);
                    mydb.SaveChanges();
                    Publisher_Load(sender, e);
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPhone.Text) || string.IsNullOrWhiteSpace(txtAddress.Text)
                || string.IsNullOrWhiteSpace(txtName.Text) || string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                MessageBox.Show("Không được để trắng dữ liệu!!!");
            }
            else if (txtPhone.Text.Length != 10)
                MessageBox.Show("Điện thoại phải có 10 số!!!");
            else
            {
                int.TryParse(txtID.Text, out int id);
                var check = mydb.Suppliers.Find(id);
                if (check == null)
                    MessageBox.Show("Nhà xuất bản này không tồn tại");
                else
                {
                    check.SupplierPhone = txtPhone.Text;
                    check.SupplierName = txtName.Text;
                    check.SupplierAddress = txtAddress.Text;
                    check.Email = txtEmail.Text;
                    if (cbbStatus.SelectedIndex == 0)
                        check.Status = 0;
                    else
                        check.Status = 1;
                    mydb.SaveChanges();
                    Publisher_Load(sender, e);
                }
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSua_Click_1(object sender, EventArgs e)
        {
            btnSua_Click(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Máy bạn phải có ổ D");
                xuatfileExcel(dgvPublisher, @"D:\", "Workbook");
                MessageBox.Show("Xuất file thành công. ", "Lưu ý", MessageBoxButtons.OK, MessageBoxIcon.Information);
                System.Diagnostics.Process.Start("Excel.exe", @"D:\Workbook.xlsx");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Xuất file không thành công", "Lưu ý", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void xuatfileExcel(DataGridView g, string duongDan, string tentep) // HÀM XUẤT FILE EXCEL
        {
            
            app obj = new app();
            obj.Application.Workbooks.Add(Type.Missing); // ĐỐI TƯỢNG OBJ ĐỂ LƯU TRỮ
            obj.Columns.ColumnWidth = 25;
            for (int i = 1; i < g.Columns.Count + 1; i++) // chạy hàng ngang
            {
                obj.Cells[1, i] = g.Columns[i - 1].HeaderText;
            }
            for (int i = 0; i < g.Rows.Count; i++)  // CHẠY HÀNG DỌC
            {
                for (int j = 0; j < g.Columns.Count; j++) // CHẠY HÀNG NGANG
                {
                    if (g.Rows[i].Cells[j].Value != null) // Khác NULL THÌ CHÈN VÀO
                    {
                        obj.Cells[i + 2, j + 1] = g.Rows[i].Cells[j].Value.ToString();
                    }
                }
            }
            obj.ActiveWorkbook.SaveCopyAs(duongDan + tentep + ".xlsx"); // LƯU TÊN TỆP
            obj.ActiveWorkbook.Saved = true;
        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            dgvNhap.DataSource = Import();
            for (int i = 0; i < dgvNhap.RowCount; i++)
            {
                int flag = 0;
                string tnxb = dgvNhap.Rows[i].Cells[1].Value.ToString();
                string phone = dgvNhap.Rows[i].Cells[2].Value.ToString();
                string address = dgvNhap.Rows[i].Cells[3].Value.ToString();
                string email = dgvNhap.Rows[i].Cells[4].Value.ToString();
                string status = dgvNhap.Rows[i].Cells[5].Value.ToString();
                if (tnxb.Length == 0 || phone.Length != 9 || address.Length == 0 || email.Length == 0 || (status != "0" && status != "1"))
                {
                    flag = 1;
                }
                if (mydb.Suppliers.FirstOrDefault(x => x.SupplierName == tnxb) != null)
                {
                    flag = 1;
                }
                if (flag == 0)
                {
                    Models.Supplier pub = new Models.Supplier();
                    pub.SupplierName = tnxb;
                    pub.SupplierPhone = "0" + phone;
                    pub.SupplierAddress = address;
                    pub.Email = email;
                    pub.Status = Convert.ToByte(status);
                    mydb.Suppliers.Add(pub);
                    mydb.SaveChanges();
                }
            }
            Publisher_Load(sender, e);
            MessageBox.Show("Nhập dữ liệu thành công!!!");
        }
        public System.Data.DataTable Import()
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Excel Workbook|*xlsx", ValidateNames = true })
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    using (var stream = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read))
                    {
                        IExcelDataReader reader;
                        if (ofd.FilterIndex == 2)
                        {
                            reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        }
                        else
                        {
                            reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        }
                        DataSet ds = new DataSet();
                        ds = reader.AsDataSet(new ExcelDataSetConfiguration()
                        {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                            {
                                UseHeaderRow = true
                            }
                        });
                        foreach (System.Data.DataTable item in ds.Tables)
                        {
                            dt = item;
                        }
                        reader.Close();
                    }
                    return dt;
                }
            }
            return null;
        }

    }
}