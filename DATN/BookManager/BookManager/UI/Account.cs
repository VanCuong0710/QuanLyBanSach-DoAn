﻿using BookManager.Models;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class Account : Form
    {
        public Account()
        {
            InitializeComponent();
        }

        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void Account_Load(object sender, EventArgs e)
        {
            var x = (from u in mydb.Accounts
                     select new
                     {
                         ID = u.AccountID,
                         UserName = u.UserName,
                         Password = u.PassWord,
                         Name = u.Name,
                         Phone = u.PhoneNumber,
                         Address = u.Address,
                         Email = u.Email,
                         Status = u.Status
                     }
                   ).ToList();
            dgvAcc.DataSource = x;
            OtherFunction.SetDataGridView(dgvAcc);
            dgvAcc.Columns[0].HeaderText ="Mã nhân viên";
            dgvAcc.Columns[1].HeaderText = "Tài khoản";
            dgvAcc.Columns[2].HeaderText = "Mật khẩu";
            dgvAcc.Columns[3].HeaderText = "Tên nhân viên";
            dgvAcc.Columns[4].HeaderText = "Số điện thoại";
            dgvAcc.Columns[5].HeaderText = "Địa chỉ";
            dgvAcc.Columns[6].HeaderText = "Email nhân viên";
            dgvAcc.Columns[7].HeaderText = "Trạng thái tài khoản";

            cbbSearch.SelectedIndex = 0;
        }

        private void dgvAcc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            txtID.Text = dgvAcc.Rows[index].Cells[0].Value.ToString();
            txtUserName.Text = dgvAcc.Rows[index].Cells[1].Value.ToString();
            txtPassword.Text = dgvAcc.Rows[index].Cells[2].Value.ToString();
            txtName.Text = dgvAcc.Rows[index].Cells[3].Value.ToString();
            txtPhone.Text = dgvAcc.Rows[index].Cells[4].Value.ToString();
            txtAddress.Text = dgvAcc.Rows[index].Cells[5].Value.ToString();
            txtEmail.Text = dgvAcc.Rows[index].Cells[6].Value.ToString();
            string x = dgvAcc.Rows[index].Cells[7].Value.ToString();
            if (x == "0")
                cbbStatus.Text = "Nhân viên";
            else if (x == "1")
                cbbStatus.Text = "Admin";
            else if (x == "2")
                cbbStatus.Text = "Bị khóa";
            txtID.Enabled = false;
            txtUserName.Enabled = false;
            txtPassword.Enabled = false;
            txtName.Enabled = false;
            txtPhone.Enabled = false;
            txtAddress.Enabled = false;
            txtEmail.Enabled = false;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            int.TryParse(txtID.Text, out int val);
            var check = mydb.Accounts.Find(val);
            if (check == null)
                MessageBox.Show("Không thấy loại tài khoản này!!!");
            else
            {
                if (cbbStatus.SelectedIndex == 0)
                    check.Status = 0;
                else if (cbbStatus.SelectedIndex == 1)
                    check.Status = 1;
                else
                    check.Status = 2;
                mydb.SaveChanges();
                Account_Load(sender, e);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtUserName.Enabled = true; txtUserName.Clear();
            txtPassword.Enabled = true; txtPassword.Clear();
            txtName.Enabled = true; txtName.Clear();
            txtPhone.Enabled = true; txtPhone.Clear();
            txtAddress.Enabled = true; txtAddress.Clear();
            txtEmail.Enabled = true; txtEmail.Clear();
            cbbStatus.SelectedIndex = 0; txtID.Clear();
            Account_Load(sender, e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtUserName.Text) || string.IsNullOrWhiteSpace(txtPassword.Text) ||
                string.IsNullOrWhiteSpace(txtName.Text) || string.IsNullOrWhiteSpace(txtPhone.Text)
                || string.IsNullOrWhiteSpace(txtAddress.Text) || string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                MessageBox.Show("Không được để trống thông tin!!!");
            }
            else
            {
                if (txtPhone.Text.Length != 10)
                    MessageBox.Show("Số điện thoại phải có 10 số !!!");
                else if (mydb.Accounts.FirstOrDefault(p => p.UserName == txtUserName.Text) != null)
                {
                    MessageBox.Show("User Name này đã tồn tại!!!");
                }
                else
                {
                    Models.Account account = new Models.Account();
                    account.UserName = txtUserName.Text;
                    account.PassWord = txtPassword.Text;
                    account.Name = txtName.Text;
                    account.PhoneNumber = txtPhone.Text;
                    account.Address = txtAddress.Text;
                    account.Email = txtEmail.Text;
                    if (cbbStatus.SelectedIndex == 0)
                        account.Status = 0;
                    else if (cbbStatus.SelectedIndex == 1)
                        account.Status = 1;
                    else
                        account.Status = 2;
                    mydb.Accounts.Add(account);
                    mydb.SaveChanges();
                    Account_Load(sender, e);
                }
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (cbbSearch.Text == "ID")
            {
                int.TryParse(txtSearch.Text, out int id);
                var x = (from u in mydb.Accounts
                         where u.AccountID == id
                         select new
                         {
                             ID = u.AccountID,
                             UserName = u.UserName,
                             Password = u.PassWord,
                             Name = u.Name,
                             Phone = u.PhoneNumber,
                             Address = u.Address,
                             Email = u.Email,
                             Status = u.Status
                         }
                   ).ToList();
                dgvAcc.DataSource = x;
            }
            if (cbbSearch.Text == "Username")
            {
                var x = (from u in mydb.Accounts
                         where u.UserName.ToLower().Contains(txtSearch.Text.ToLower())
                         select new
                         {
                             ID = u.AccountID,
                             UserName = u.UserName,
                             Password = u.PassWord,
                             Name = u.Name,
                             Phone = u.PhoneNumber,
                             Address = u.Address,
                             Email = u.Email,
                             Status = u.Status
                         }
                   ).ToList();
                dgvAcc.DataSource = x;
            }
            if (cbbSearch.Text == "Tên")
            {
                var x = (from u in mydb.Accounts
                         where u.Name.ToLower().Contains(txtSearch.Text.ToLower())
                         select new
                         {
                             ID = u.AccountID,
                             UserName = u.UserName,
                             Password = u.PassWord,
                             Name = u.Name,
                             Phone = u.PhoneNumber,
                             Address = u.Address,
                             Email = u.Email,
                             Status = u.Status
                         }
                    ).ToList();
                dgvAcc.DataSource = x;
            }
        }
    }
}