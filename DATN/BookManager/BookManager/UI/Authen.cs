﻿using BookManager.Models;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class Authen : Form
    {
        public Authen()
        {
            InitializeComponent();
        }

        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void Authen_Load(object sender, EventArgs e)
        {
        }

        private void chkMk_Click(object sender, EventArgs e)
        {
            if (chkMk.Checked)
            {
                txtMk.PasswordChar = '\0';
                chkMk.ForeColor = Color.Blue;
            }
            else
            {
                txtMk.PasswordChar = '*';
                chkMk.ForeColor = Color.Black;
            }
        }

        /// <summary>
        /// nút đăng nhập
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            var account = mydb.Accounts.FirstOrDefault(p => p.UserName == txtTk.Text && p.PassWord == txtMk.Text);
            if (account == null)
            {
                MessageBox.Show("Tài khoản và mật khẩu bạn nhập không chính xác!!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(account.Status==2)
            {
                MessageBox.Show("Tài khoản này đã bị khóa!!!");
            }    
            else
            {
                OTP otp = new OTP();
                otp.Email = account.Email;
                otp.ID = account.AccountID;
                this.Hide();
                otp.ShowDialog();
                this.Close();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            GetPassword get = new GetPassword();
            get.Show();
        }
    }
}