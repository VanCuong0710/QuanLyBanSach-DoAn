﻿using BookManager.Models;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class Export : Form
    {
        public int ID { get; set; }

        public Export()
        {
            InitializeComponent();
        }

        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void Export_Load(object sender, EventArgs e)
        {
            cbbType.SelectedIndex = 1;
            var dskh = (from u in mydb.Clients
                        select new
                        {
                            ID = u.ClientID
                            ,
                            Name = u.ClientName
                            ,
                            Phone = u.PhoneNumber
                            ,
                            Address = u.ClientAddress
                            ,
                            Email = u.Email
                        }).ToList();
            dgvKh.DataSource = dskh;
            OtherFunction.SetDataGridView(dgvKh);
            dgvKh.Columns[0].HeaderText = "Mã khách hàng";
            dgvKh.Columns[1].HeaderText = "Tên khách hàng";
            dgvKh.Columns[2].HeaderText = "Điện thoại khách hàng";
            dgvKh.Columns[3].HeaderText = "Địa chỉ khách hàng";
            dgvKh.Columns[4].HeaderText = "Email khách hàng";

        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void dgvKh_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (cbbType.Text == "Cũ")
            {
                try
                {
                    txtSdt.Text = dgvKh.Rows[index].Cells[2].Value.ToString();
                    txtDc.Text = dgvKh.Rows[index].Cells[3].Value.ToString();
                    txtTenkh.Text = dgvKh.Rows[index].Cells[1].Value.ToString();
                    txtMail.Text = dgvKh.Rows[index].Cells[4].Value.ToString();
                }
                catch (Exception)
                {
                }
            }
        }

        private void btnTaoHoaDon_Click(object sender, EventArgs e)
        {
            if (cbbType.Text == "Mới")
            {
                if (string.IsNullOrWhiteSpace(txtAddress.Text) || string.IsNullOrWhiteSpace(txtPhone.Text) || string.IsNullOrWhiteSpace(txtName.Text)
                    || string.IsNullOrWhiteSpace(txtEmail.Text))
                {
                    MessageBox.Show("Không được để trống dữ liệu !!!");
                }
                else
                {
                    Client client = new Client();
                    if (txtPhone.TextLength != 10)
                    {
                        MessageBox.Show("Số điện thoại phải có 10 số");
                    }
                    else
                    {
                        client.Email = txtEmail.Text;
                        client.ClientAddress = txtAddress.Text;
                        client.ClientName = txtName.Text;
                        client.PhoneNumber = txtPhone.Text;
                        if (mydb.Clients.Where(p => p.PhoneNumber == client.PhoneNumber).FirstOrDefault() == null)
                        {
                            mydb.Clients.Add(client);
                            mydb.SaveChanges();
                            ExportDetail f = new ExportDetail();
                            f.TopLevel = false;
                            f.TopMost = true;
                            f.Dock = DockStyle.Fill;
                            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
                            this.Parent.Controls.Add(f);
                            this.Parent.Controls.Remove(this);
                            f.AccountID = ID;
                            f.ClientID = client.ClientID;
                            f.Show();
                        }
                        else
                        {
                            MessageBox.Show("Khách hàng này đã tồn tại");
                        }
                    }
                }
            }
            else if ((cbbType.Text == "Cũ"))
            {
                if (string.IsNullOrWhiteSpace(txtDc.Text) || string.IsNullOrWhiteSpace(txtSdt.Text) || string.IsNullOrWhiteSpace(txtMail.Text)
                  || string.IsNullOrWhiteSpace(txtTenkh.Text))
                {
                    MessageBox.Show("Không được để trống dữ liệu !!!");
                }
                else
                {
                    var client = mydb.Clients.Where(p => p.PhoneNumber == txtSdt.Text && p.ClientName==txtTenkh.Text).FirstOrDefault();
                    ExportDetail f = new ExportDetail();
                    f.TopLevel = false;
                    f.TopMost = true;
                    f.Dock = DockStyle.Fill;
                    f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
                    this.Parent.Controls.Add(f);
                    this.Parent.Controls.Remove(this);
                    f.AccountID = ID;
                    f.ClientID = client.ClientID;
                    f.Show();
                }
            }
        }

        private void txtSdt_TextChanged(object sender, EventArgs e)
        {
            var dskh = (from u in mydb.Clients
                        where u.PhoneNumber.Contains(txtSdt.Text)
                        select new
                        {
                            ID = u.ClientID
                            ,
                            Name = u.ClientName
                            ,
                            Phone = u.PhoneNumber
                            ,
                            Address = u.ClientAddress
                            ,
                            Email = u.Email
                        }).ToList();
            dgvKh.DataSource = dskh;
            if (mydb.Clients.Where(p => p.PhoneNumber == txtSdt.Text).FirstOrDefault() != null)
            {
                var client = mydb.Clients.Where(p => p.PhoneNumber == txtSdt.Text).FirstOrDefault();
                txtTenkh.Text = client.ClientName;
                txtMail.Text = client.Email;
                txtDc.Text = client.ClientAddress;
            }
        }

        private void cbbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbType.Text == "Mới")
            {
                grNew.Visible = true;
                grOld.Visible = false;
            }
            else
            {
                grOld.Visible = true;
                grNew.Visible = false;
            }
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtAddress.Clear();
            txtDc.Clear();
            txtEmail.Clear();
            txtMail.Clear();
            txtName.Clear();
            txtPhone.Clear();
            txtTenkh.Clear();
            txtSdt.Clear();
        }

        private void txtTenkh_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Inventory inventory=new Inventory();
            inventory.Test = 1;
            inventory.Show();
        }
    }
}