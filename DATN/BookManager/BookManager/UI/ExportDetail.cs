﻿using BookManager.DetailBill;
using BookManager.Models;
using BookManager.OtherClass;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class ExportDetail : Form
    {
        private int tongtien = 0;
        public int ClientID { get; set; } = 1;
        public int AccountID { get; set; } = 1;
        private ManagementBookEntities mydb = new ManagementBookEntities();

        public ExportDetail()
        {
            InitializeComponent();
        }

        private void ExportDetail_Load(object sender, EventArgs e)
        {
            txtMhd.Text = (mydb.BillOfSales.Count() + 1).ToString();
            txtNguoiLap.Text = (mydb.Accounts.Find(AccountID).Name);
            var client = (mydb.Clients.Find(ClientID));
            txtSdt.Text = client.PhoneNumber;
            txtTenKH.Text = client.ClientName;
            txtDiaChi.Text = client.ClientAddress;

            var listHangs = from u in mydb.Books
                            where u.Status == 0
                            select u;
            cbbHang.DataSource = listHangs.ToList();
            cbbHang.DisplayMember = "BookName";
            cbbHang.ValueMember = "BookID";
            if (int.TryParse(cbbHang.SelectedValue.ToString(), out int id) == true)
            {
                var book = mydb.Books.Find(id);
                txtGia.Text = book.Price.ToString();
                txtTheLoai.Text = book.Category.CategoryName;
                txtSoLuong.Text = book.Quantity.ToString();
            }

            OtherFunction.SetDataGridView(dgvDshang);
            OtherFunction.SetFormatMoney(dgvDshang, 4);
            OtherFunction.SetFormatMoney(dgvDshang, 5);
            tinhTongTien();
        }

        private void cbbHang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.TryParse(cbbHang.SelectedValue.ToString(), out int id) == true)
            {
                var book = mydb.Books.Find(id);
                txtGia.Text = book.Price.ToString();
                txtTheLoai.Text = book.Category.CategoryName;
                txtSoLuong.Text = book.Quantity.ToString();
            }
        }

        private void txtSoLuongBan_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Thêm hàng vào datagrid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSoLuongBan.Text))
            {
                MessageBox.Show("Không được để trắng số lượng bán");
            }
            else if (Convert.ToInt32(txtSoLuongBan.Text) > Convert.ToInt32(txtSoLuong.Text))
            {
                MessageBox.Show("Không còn hàng để bán");
            }
            else
            {
                bool checkHang = true;
                BookSales bookSales = new BookSales();
                bookSales.ID = Convert.ToInt32(cbbHang.SelectedValue);
                bookSales.Name = cbbHang.Text;
                bookSales.Price = Convert.ToInt32(txtGia.Text);
                bookSales.Category = txtTheLoai.Text;
                bookSales.Quantity = Convert.ToInt32(txtSoLuongBan.Text);
                for (int i = 0; i < dgvDshang.RowCount; i++)
                {
                    if (bookSales.ID == Convert.ToInt32(dgvDshang.Rows[i].Cells[0].Value.ToString()))
                    {
                        checkHang = false;
                        break;
                    }
                }
                if (checkHang == true)
                {
                    dgvDshang.Rows.Add(bookSales.ID, bookSales.Name, bookSales.Category, bookSales.Quantity, bookSales.Price, bookSales.TotalPrice);
                }
                else
                {
                    MessageBox.Show("Hàng đã tồn tại không thể thêm");
                }
            }
            tinhTongTien();
        }

        /// <summary>
        /// Hủy hóa đơn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Thanh toán
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnThanhToan_Click(object sender, EventArgs e)
        {
            if (dgvDshang.RowCount == 0)
            {
                MessageBox.Show("Danh sách hàng chưa có sản phẩm nào!!!");
            }
            else
            {
                DialogResult kq = MessageBox.Show("Vui lòng kiểm tra kỹ thông tin", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (kq == DialogResult.Yes)
                {
                    BillOfSale billOfSale = new BillOfSale();
                    billOfSale.ClientID = ClientID;
                    billOfSale.AccountID = AccountID;
                    billOfSale.DateSale = dtpSale.Value;
                    mydb.BillOfSales.Add(billOfSale);
                    mydb.SaveChanges();

                    for (int i = 0; i < dgvDshang.Rows.Count; i++)
                    {
                        BillDetail billDetail = new BillDetail();
                        billDetail.TransactionID = billOfSale.TransactionID;
                        billDetail.BookID = Convert.ToInt32(dgvDshang.Rows[i].Cells[0].Value.ToString());
                        billDetail.Quantity = Convert.ToInt32(dgvDshang.Rows[i].Cells[3].Value.ToString());
                        var book = mydb.Books.Find(billDetail.BookID);
                        book.Quantity = Convert.ToInt32(book.Quantity - billDetail.Quantity);
                        billDetail.SalePrice = Convert.ToInt32(dgvDshang.Rows[i].Cells[4].Value.ToString());
                        mydb.BillDetails.Add(billDetail);
                        mydb.SaveChanges();
                    }
                    MessageBox.Show("Tạo hóa đơn thành công!!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    await Task.Run(() => Mail.SendMail(billOfSale.TransactionID));
                    DetailExportedBill detail = new DetailExportedBill();
                    detail.TransactionID = mydb.BillOfSales.Count();
                    detail.Show();
                }
                
            }
        }

        /// <summary>
        /// Chọn hàng để xóa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvDshang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                cbbHang.Text = dgvDshang.Rows[index].Cells[1].Value.ToString();
            }
        }

        /// <summary>
        /// Xóa hàng từ datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnXoa_Click(object sender, EventArgs e)
        {
            int? index = null;
            for (int i = 0; i < dgvDshang.RowCount; i++)
            {
                if (cbbHang.SelectedValue.ToString() == dgvDshang.Rows[i].Cells[0].Value.ToString())
                {
                    index = i;
                    break;
                }
            }
            if (index == null)
            {
                MessageBox.Show("Vui lòng chọn hàng để xóa");
            }
            else
            {
                dgvDshang.Rows.RemoveAt(Convert.ToInt32(index));
            }
            tinhTongTien();
        }

        /// <summary>
        /// Tính tổng tiền
        /// </summary>
        private void tinhTongTien()
        {
            tongtien = 0;
            for (int i = 0; i < dgvDshang.RowCount; i++)
            {
                string tien = dgvDshang.Rows[i].Cells[5].Value.ToString();
                tien = tien.Replace("VNĐ", "");
                tien = tien.Replace(" ", "");
                tongtien = tongtien + Convert.ToInt32(tien);
            }
            txtTongTien.Text = tongtien.ToString();
            OtherFunction.FormatTextbox(txtTongTien);
        }

        private void txtCk_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
    }
}