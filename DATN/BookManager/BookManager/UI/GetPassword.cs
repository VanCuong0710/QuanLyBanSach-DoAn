﻿using BookManager.Models;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class GetPassword : Form
    {
        public GetPassword()
        {
            InitializeComponent();
        }

        private ManagementBookEntities mydb = new ManagementBookEntities();
        private string randomCode;

        private async void btnGet_Click(object sender, EventArgs e)
        {
            if (GetAccount() == true)
            {
                MessageBox.Show("OTP đã được gửi đến email: " + txtEmail.Text);
                txtOTP.Visible = true;
                btnAgree.Visible = true;
                label3.Visible = true;
                await Task.Run(() => Send());
            }
            else
                MessageBox.Show("Không tìm thấy tài khoản");
        }

        protected string Generate_otp()
        {
            char[] charArr = "0123456789".ToCharArray();
            string strrandom = string.Empty;
            Random objran = new Random();
            for (int i = 0; i < 4; i++)
            {
                //It will not allow Repetation of Characters
                int pos = objran.Next(1, charArr.Length);
                if (!strrandom.Contains(charArr.GetValue(pos).ToString())) strrandom += charArr.GetValue(pos);
                else i--;
            }
            return strrandom;
        }

        private void Send()
        {
            string from, pass, messageBody;
            Random rand = new Random();
            randomCode = rand.Next(999999).ToString();
            MailMessage message = new MailMessage();
            string to = txtEmail.Text;
            from = "cuongcalmtodoit@gmail.com";
            pass = "iiluimgeoiinqxih";
            messageBody = "OTP: " + randomCode;
            message.To.Add(to);
            message.From = new MailAddress(from);
            message.Body = messageBody;
            message.Subject = "[MiniStop] OTP";
            SmtpClient smt = new SmtpClient("smtp.gmail.com");
            smt.EnableSsl = true;
            smt.Port = 587;
            smt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smt.Credentials = new NetworkCredential(from, pass);
            try
            {
                smt.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private bool GetAccount()
        {
            var account = mydb.Accounts.Where(x => x.UserName == txtUsername.Text && x.Email == txtEmail.Text).FirstOrDefault();
            if (account == null)
                return false;
            return true;
        }

        private void btnAgree_Click(object sender, EventArgs e)
        {
            var account = mydb.Accounts.Where(x => x.UserName == txtUsername.Text && x.Email == txtEmail.Text).FirstOrDefault();
            if (txtOTP.Text == randomCode)
            {
                MessageBox.Show("Thành công!!! Mật khẩu của bạn là: " + account.PassWord);
            }
            else
            {
                MessageBox.Show("OTP không đúng!!!");
            }
        }
    }
}