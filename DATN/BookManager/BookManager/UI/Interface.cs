﻿using BookManager.Models;
using BookManager.Statistics;
using System;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class Interface : Form
    {
        public int ID { get; set; } = 1;
        private ManagementBookEntities mydb = new ManagementBookEntities();

        public Interface()
        {
            InitializeComponent();
        }

        private string ChucVu()
        {
            var account = mydb.Accounts.Find(ID);
            if (account.Status == 0)
            {
                return "Nhân viên";
            }
            return "Quản lý";
        }

        private void Interface_Load(object sender, EventArgs e)
        {
            var account = mydb.Accounts.Find(ID);
            lblGreet.Text = $"Xin chào {account.Name} với cấp bậc: " + ChucVu();
            if (account.Status == 0)
                button4.Enabled = false;
            else
                button4.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            Export f = new Export();
            f.ID = this.ID;
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void btnKho_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            Inventory f = new Inventory();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button2.Visible == false)
            {
                button3.Visible = true;
                button2.Visible = true;
            }
            else if (button2.Visible == true)
            {
                button3.Visible = false;
                button2.Visible = false;
            }
        }

        private void btnNxb_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            Publisher f = new Publisher();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            Account f = new Account();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            ModifyInformation f = new ModifyInformation();
            f.ID = this.ID;
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            _Import f = new _Import();
            f.ID = this.ID;
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            SatImportedBill f = new SatImportedBill();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            SatExportedBill f = new SatExportedBill();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {

            this.pnlDisplay.Controls.Clear();
            ClientVip f = new ClientVip();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            Profit f = new Profit();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            BestSeller f = new BestSeller();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (btnKho.Visible == true)
            {
                btnKho.Visible = false;
                button5.Visible = false;
                btnNxb.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button10.Visible = false;
                button6.Visible = false;
                button11.Visible = false;
                button12.Visible = false;

            }
            else if(btnKho.Visible == false)
            {
                btnKho.Visible = true;
                button5.Visible = true;
                btnNxb.Visible = true;
                button7.Visible = true;
                button8.Visible = true;
                button10.Visible = true;
                button6.Visible = true;
                button11.Visible = true;
                button12.Visible = true;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button14_Click(object sender, EventArgs e)
        {
            Authen authen = new Authen();
            this.Hide();
            authen.ShowDialog();
            this.Close();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.pnlDisplay.Controls.Clear();
            KPI f = new KPI();
            f.TopLevel = false;
            f.TopMost = true;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = (FormBorderStyle)FormBorderStyle.None;
            this.pnlDisplay.Controls.Add(f);
            f.Show();
        }
    }
}