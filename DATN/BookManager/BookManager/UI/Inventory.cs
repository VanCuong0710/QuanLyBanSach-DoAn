﻿// <copyright file="Inventory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookManager.UI
{
    using BookManager.Models;
    using System;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Form Inventory.
    /// </summary>
    public partial class Inventory : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Inventory"/> class.
        /// Where starts inventory.
        /// </summary>
        public int Test { get; set; }
        public Inventory()
        {
            this.InitializeComponent();
        }

        private readonly ManagementBookEntities mydb = new ManagementBookEntities();

        private void Inventory_Load(object sender, EventArgs e)
        {
            var dsHang = from u in this.mydb.Books
                         select new
                         {
                             ID = u.BookID,
                             Name = u.BookName,
                             Status = u.Status,
                             Category = u.Category.CategoryName,
                             Quantiy = u.Quantity,
                             Price = u.Price,
                         };
            this.dgvDshang.DataSource = dsHang.ToList();
            dgvDshang.Columns[0].HeaderText = "Mã sách";
            dgvDshang.Columns[1].HeaderText = "Tên sách";
            dgvDshang.Columns[2].HeaderText = "Trạng thái sách";
            dgvDshang.Columns[3].HeaderText = "Loại sách";
            dgvDshang.Columns[4].HeaderText = "Số lượng";
            dgvDshang.Columns[5].HeaderText = "Giá";

            OtherFunction.SetDataGridView(this.dgvDshang);
            OtherFunction.SetFormatMoney(this.dgvDshang, 5);
            OtherFunction.FormatNumber(this.dgvDshang, 4);
            this.cbbStatus.SelectedIndex = 0;

            var dsCategory = (from u in this.mydb.Categories select u).ToList();
            this.cbbCategory.DataSource = dsCategory;
            this.cbbCategory.DisplayMember = "CategoryName";
            this.cbbCategory.ValueMember = "CategoryID";

            if(Test==1)
            {
                btnAdd.Enabled = false;
                btnSua.Enabled = false;
            }    
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvDshang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int d = e.RowIndex;
                this.txtID.Text = this.dgvDshang.Rows[d].Cells[0].Value.ToString();
                this.txtTen.Text = this.dgvDshang.Rows[d].Cells[1].Value.ToString();
                if (this.dgvDshang.Rows[d].Cells[2].Value.ToString() == "0")
                {
                    this.cbbStatus.SelectedIndex = 0;
                }
                else
                {
                    this.cbbStatus.SelectedIndex = 1;
                }

                this.cbbCategory.Text = this.dgvDshang.Rows[d].Cells[3].Value.ToString();
                this.txtSl.Text = this.dgvDshang.Rows[d].Cells[4].Value.ToString().Replace(" ", string.Empty);
                this.txtPrice.Text = this.dgvDshang.Rows[d].Cells[5].Value.ToString().
                    Replace(" ", string.Empty).Replace("VNĐ", string.Empty);
            }
            catch (Exception)
            {

            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtSl.Clear();
            this.txtPrice.Clear();
            this.txtTen.Clear();
            this.txtID.Clear();
            this.cbbCategory.SelectedIndex = 0;
            this.cbbStatus.SelectedIndex = 0;
            Inventory_Load(sender, e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            if (this.cbbStatus.Text == "Hàng vẫn nhập")
            {
                book.Status = 0;
            }
            else
            {
                book.Status = 1;
            }

            if (string.IsNullOrWhiteSpace(this.txtSl.Text) || string.IsNullOrWhiteSpace(this.txtPrice.Text) ||
                string.IsNullOrWhiteSpace(this.txtTen.Text))
            {
                MessageBox.Show("Không được để trống dữ liệu");
            }
            else
            {
                if (this.mydb.Books.Where(x => x.BookName == this.txtTen.Text).FirstOrDefault() == null)
                {
                    book.CategoryID = Convert.ToInt32(this.cbbCategory.SelectedValue);
                    book.BookName = this.txtTen.Text;
                    book.Quantity = Convert.ToInt32(this.txtSl.Text);
                    book.Price = Convert.ToInt32(this.txtPrice.Text);
                    this.mydb.Books.Add(book);
                    this.mydb.SaveChanges();
                    this.Inventory_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("Sách này đã tồn tại");
                }
            }
        }

        private void txtSl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            int.TryParse(this.txtID.Text, out int i);
            if (i <= 0)
            {
                MessageBox.Show("Không tìm thấy sách này");
            }
            else
            {
                Book book = this.mydb.Books.Find(i);
                if (this.cbbStatus.Text == "Hàng vẫn nhập")
                {
                    book.Status = 0;
                }
                else
                {
                    book.Status = 1;
                }

                if (string.IsNullOrWhiteSpace(this.txtSl.Text) || string.IsNullOrWhiteSpace(this.txtPrice.Text) ||
                    string.IsNullOrWhiteSpace(this.txtTen.Text))
                {
                    MessageBox.Show("Không được để trống dữ liệu");
                }
                else
                {
                    book.CategoryID = Convert.ToInt32(this.cbbCategory.SelectedValue);
                    book.BookName = this.txtTen.Text;
                    book.Quantity = Convert.ToInt32(this.txtSl.Text);
                    book.Price = Convert.ToInt32(this.txtPrice.Text);
                    this.mydb.SaveChanges();
                    this.Inventory_Load(sender, e);
                }
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            var dsHang = from u in this.mydb.Books
                         where u.BookName.ToUpper().Contains(txtSearch.Text.ToUpper())
                         select new
                         {
                             ID = u.BookID,
                             Name = u.BookName,
                             Status = u.Status,
                             Category = u.Category.CategoryName,
                             Quantiy = u.Quantity,
                             Price = u.Price,
                         };
            this.dgvDshang.DataSource = dsHang.ToList();
        }
    }
}