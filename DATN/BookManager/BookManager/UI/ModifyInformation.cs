﻿using BookManager.Models;
using System;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class ModifyInformation : Form
    {
        public int ID { get; set; } = 1;

        public ModifyInformation()
        {
            InitializeComponent();
        }

        private ManagementBookEntities mydb = new ManagementBookEntities();

        private void ModifyInformation_Load(object sender, EventArgs e)
        {
            var check = mydb.Accounts.Find(ID);
            txtID.Text = ID.ToString();
            txtAddress.Text = check.Address;
            txtName.Text = check.Name;
            txtUserName.Text = check.UserName;
            txtEmail.Text = check.Email;
            txtPassword.Text = check.PassWord;
            txtPhone.Text = check.PhoneNumber;
            if (check.Status == 0)
                txtStatus.Text = "Nhân viên";
            if (check.Status == 1)
                txtStatus.Text = "Admin";
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtName.Text) || string.IsNullOrWhiteSpace(txtPassword.Text) ||
                string.IsNullOrWhiteSpace(txtPhone.Text) || string.IsNullOrWhiteSpace(txtAddress.Text) ||
                string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                MessageBox.Show("Không được để trắng!!!");
            }
            else
            {
                if (txtPhone.Text.Length != 10)
                    MessageBox.Show("Số điện thoại phải có 10 số!!!");
                else
                {
                    var check = mydb.Accounts.Find(ID);
                    check.Address = txtAddress.Text;
                    check.Name = txtName.Text;
                    check.Email = txtEmail.Text;
                    check.PassWord = txtPassword.Text;
                    check.PhoneNumber = txtPhone.Text;
                    mydb.SaveChanges();
                    ModifyInformation_Load(sender, e);
                    MessageBox.Show("Thông tin đã được thay đổi");
                }
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
    }
}