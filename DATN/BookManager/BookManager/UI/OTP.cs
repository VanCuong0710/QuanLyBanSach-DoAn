﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManager.UI
{
    public partial class OTP : Form
    {
        public int ID { get; set; }
        public string Email { get; set; }
        private string randomCode;

        public OTP()
        {
            InitializeComponent();
        }
        /// <summary>
        /// TẠO MÃ OTP GỒM 6 CHỮ
        /// </summary>
        /// <returns></returns>
        protected string Generate_otp()
        {
            char[] charArr = "0123456789".ToCharArray();
            string strrandom = string.Empty;
            Random objran = new Random();
            for (int i = 0; i < 4; i++)
            {
                //It will not allow Repetation of Characters
                int pos = objran.Next(1, charArr.Length);
                if (!strrandom.Contains(charArr.GetValue(pos).ToString())) strrandom += charArr.GetValue(pos);
                else i--;
            }
            return strrandom;
        }

        private async void OTP_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "OTP đã được gửi đến gmail:\n" + Email;
            await Task.Run(() => Send());
        }

        private void Send()
        {
            string from, pass, messageBody;
            Random rand = new Random();
            randomCode = rand.Next(999999).ToString();
            MailMessage message = new MailMessage();
            string to = Email;
            from = "cuongcalmtodoit@gmail.com";
            pass = "iiluimgeoiinqxih";
            messageBody = "OTP: " + randomCode;
            message.To.Add(to);
            message.From = new MailAddress(from);
            message.Body = messageBody;
            message.Subject = "[MiniStop] OTP";
            SmtpClient smt = new SmtpClient("smtp.gmail.com");
            smt.EnableSsl = true;
            smt.Port = 587;
            smt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smt.Credentials = new NetworkCredential(from, pass);
            try
            {
                smt.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnXN_Click(object sender, EventArgs e)
        {
            if (txtOTP.Text == randomCode)
            {
                MessageBox.Show("Thành công!!!");
                this.Hide();
                Interface @interface = new Interface();
                @interface.ID = this.ID;
                @interface.ShowDialog();
            }
            else
            {
                MessageBox.Show("OTP không đúng!!!");
                this.Hide();
                Authen authen = new Authen();
                authen.ShowDialog();
            }
        }
    }
}